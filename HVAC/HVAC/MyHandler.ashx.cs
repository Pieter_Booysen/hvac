﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft;
using System.Web.Script.Serialization;
using System.Text;
using Microsoft.ApplicationBlocks;
using System.Web.Services;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace HVAC
{
    /// <summary>
    /// Summary description for MyHandler
    /// </summary>
    [Serializable]
    public class MyHandler : IHttpHandler, IRequiresSessionState
    {

        DataAccessLayer datalayer = new DataAccessLayer();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (context.Request["action"].ToString() != "")
            {
                switch (context.Request["action"].ToString())
                {
                    case "testconnection":
                        {
                            context.Response.Write("Test connection");
                            break;
                        }

                    case "getNC_HHFailed":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_HHFailed(sdate, siteid));
                            break;
                        }
                    case "getNC_GetNurseRound":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_GetNurseRound(sdate, siteid));
                            break;
                        }
                    case "getNC_CallNotAttentEmergencyCancel_Linechart":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_CallNotAttentEmergencyCancel_Linechart(sdate, siteid));
                            break;
                        }
                    case "getNC_CallElevateEmergencyCancel_Linechart":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_CallElevateEmergencyCancel_Linechart(sdate, siteid));
                            break;
                        }

                    case "getNC_EStartEndCall_Linechart":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_EStartEndCall_Linechart(sdate, siteid));
                            break;
                        }

                    case "getNC_StartEndCall_Linechart":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_StartEndCall_Linechart(sdate, siteid));
                            break;
                        }

                    case "getNC_EmergencyStartEndCall":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_EStartEndCall(sdate, siteid));
                            break;
                        }
                    case "getNC_StartEndCall":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_StartEndCall(sdate, siteid));
                            break;
                        }

                    case "getNC_CallNotAttentEmergencyCancel":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_CallNotAttentEmergencyCancel(sdate, siteid));
                            break;
                        }

                    case "getNC_CallElevateEmergencyCancel":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            //string siteid = context.Request["siteid"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getNC_CallElevateEmergencyCancel(sdate, siteid));
                            break;
                        }

                    case "gettaskToday":
                        {
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(gettaskToday(siteid));
                            break;
                        }
                    case "getactivitiesToday":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getactivitiesToday(sdate, siteid));
                            break;
                        }
                    case "getactivitiesLatest":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getactivitiesLatest(sdate, siteid));
                            break;
                        }
                    case "gettaskLatest":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(gettaskLatest(sdate, siteid));
                            break;
                        }
                    case "getBedCalls":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getchartBedCalls(sdate, siteid));
                            break;
                        }
                    case "getchartFaultyhandheld":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.ContentType = "application/json; charset=utf-8";
                            context.Response.Write(getchartFaultyhandheld(sdate, siteid));
                            break;
                        }
                    case "getchartECalls":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1";//Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getChartECallsDaily(sdate, siteid));
                            break;
                        }
                    case "getchartCallNot":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            context.Response.Write(getChartCallNotDaily(sdate, siteid));
                            break;
                        }
                    case "getAllCallCount":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1";//Sessions.getSession().site.ID.ToString();
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(getCalls(sdate, siteid), x);
                            context.Response.Write(strings);
                            break;
                        }
                    case "getCallCountActivities":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            object getinfo = datalayer.getCallCountActivities(sdate);
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                            context.Response.Write(strings);
                            break;
                        }
                    case "getECallCountActivities":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            object getinfo = datalayer.getECallCountActivities(sdate);
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                            context.Response.Write(strings);
                            break;
                        }
                    case "getCallToECall":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            object getinfo = datalayer.getCallToECall(sdate);
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                            context.Response.Write(strings);
                            break;
                        }
                    case "getCallnotattended":
                        {
                            string sdate = context.Request["sdate"].ToString();
                            string siteid = "1"; //Sessions.getSession().site.ID.ToString();
                            object getinfo = datalayer.getCallnotattended(sdate);
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                            context.Response.Write(strings);
                            break;
                        }

                    case "login":
                        {
                            string username = context.Request["username"].ToString();
                            string password = context.Request["password"].ToString();

                            int isitecount = 0; // Used as out parameter.
                            Sessions s = Sessions.getSession(context);
                            try
                            {

                                if (s.login(username, password, out isitecount))
                                {
                                    if (isitecount == 0)
                                    {
                                        context.Response.Write("0");
                                    }
                                    else if (isitecount == 1)
                                    {

                                        object oSites = "1"; //NATS.mSite.getSitesPerUserID(Sessions.getSession().user.ID);
                                        Sessions.getSession().setSite(Convert.ToInt16(oSites));

                                        context.Response.Write("1");
                                    }
                                    else if (isitecount > 2)
                                    {
                                        context.Response.Write("2");
                                    }
                                }
                                else
                                {
                                    context.Response.Write("0");
                                }
                            }
                            catch (Exception ex)
                            {
                                context.Response.Write("-1");
                            }
                            break;
                        }
                    case "setSite":
                        {
                            int iSiteselected = String.IsNullOrEmpty(context.Request["siteid"].ToString()) ? 0 : Convert.ToInt16(context.Request["siteid"].ToString());

                            if (iSiteselected > 0)
                            {
                                Sessions.getSession().setSite(iSiteselected);
                                //NATS.mSite.getSitesPerUser(Sessions.getSession().user.ID);
                                context.Response.Write("true");
                            }
                            else
                            {
                                context.Response.Write("false");
                            }
                            break;
                        }
                    case "getSites":   //changes
                        {

                            object oSites = "1"; //NATS.mSite.getSitesPerUser(Sessions.getSession().user.ID);
                            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(oSites, x);
                            //string daresult = DataSetToJSON(oSites);
                            context.Response.Write(strings);
                            break;
                        }
                    case "gettestdata":
                        {
                            context.Response.Write(getAllEq());
                            break;
                        }
                    case "gettestdata2":
                        {
                            context.Response.Write(getdbdata());
                            break;
                        }
                    case "GetTableData":
                        {
                            // Those parameters are sent by the plugin
                            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
                            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
                            var iSortCol = int.Parse(context.Request["iSortCol_0"]);
                            var iSortDir = context.Request["sSortDir_0"];

                            //HCSEntities ent = new HCSEntities();
                            //ent.Configuration.ProxyCreationEnabled = false;
                            //// Fetch the data from a repository (in my case in-memory)
                            //var product = ent.Stations.OrderBy(p => p.Name).ToList();
                            string query = "";
                            //IEnumerable<Station> query = ent.Stations.ToList();
                            //Func<Station, object> order = p =>
                            //{
                            //    if (iSortCol == 0)
                            //    {
                            //        return p.ID;
                            //    }
                            //    return p.Name;
                            //};

                            // Define the order direction based on the iSortDir parameter
                            //if ("desc" == iSortDir)
                            //{
                            //    query = query.OrderByDescending(order);
                            //}
                            //else
                            //{
                            //    query = query.OrderBy(order);
                            //}
                            //// prepare an anonymous object for JSON serializationss
                            //var result = new
                            //{
                            //    iTotalRecords = query.Count(),
                            //    iTotalDisplayRecords = query.Count(),
                            //    aaData = query
                            //        .Select(p => new[] { p.ID.ToString(), p.Name })
                            //        .Skip(iDisplayStart)
                            //        .Take(iDisplayLength)
                            //};
                            string result = "";
                            var serializer = new JavaScriptSerializer();
                            var json = serializer.Serialize(result);
                            context.Response.ContentType = "application/json";
                            context.Response.Write(json);

                            //context.Response.Write(GetTableData());
                            break;
                        }
                }
            }
        }


        #region General

        #endregion

        #region SQLSP


        private string getNC_HHFailed(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_HHFailed", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_HHFailed", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_GetNurseRound(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_NureRound", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_NureRound", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_EStartEndCall(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_EStartEndCall", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_EStartEndCall", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_StartEndCall(string sdate, string siteid)//100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_StartEndCall", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_StartEndCall", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_CallNotAttentEmergencyCancel(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallNotAttentEmergencyCancel", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallNotAttentEmergencyCancel", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_CallElevateEmergencyCancel(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallElevateEmergencyCancel", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallElevateEmergencyCancel", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_CallNotAttentEmergencyCancel_Linechart(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallNotAttentEmergencyCancel_Linechart", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallNotAttentEmergencyCancel_Linechart", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_CallElevateEmergencyCancel_Linechart(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallElevateEmergencyCancel_Linechart", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_CallElevateEmergencyCancel_Linechart", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_EStartEndCall_Linechart(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_EStartEndCall_Linechart", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_EStartEndCall_Linechart", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getNC_StartEndCall_Linechart(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_StartEndCall_Linechart", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getNC_StartEndCall_Linechart", dtFrom, dtTo, siteid);

                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0], x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;

            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string gettaskToday(string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "gettaskToday", siteid);

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string gettaskLatest(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1";// Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "gettaskLatest", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "gettaskLatest", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        public string getactivitiesToday(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getactivitiesToday", siteid);

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getactivitiesLatest(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getactivitiesLatest", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getactivitiesLatest", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getchartFaultyhandheld(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1";// Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getchartFaultyhandheld", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getchartFaultyhandheld", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getCalls(string Date, string siteid)
        {
            try
            {
                string strCallinfo = "";
                string strings = "";
                object getinfo;
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                //Call
                getinfo = datalayer.getCallCountActivities(Date);
                strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                strCallinfo = strings;
                //ECall
                getinfo = datalayer.getECallCountActivities(Date);
                strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                strCallinfo += "," + strings;
                //NotAttendCall
                getinfo = datalayer.getCallnotattended(Date);
                strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                strCallinfo += "," + strings;
                //CalltoEcall
                getinfo = datalayer.getCallToECall(Date);
                strings = Newtonsoft.Json.JsonConvert.SerializeObject(getinfo, x);
                strCallinfo += "," + strings;
                return strCallinfo;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getchartBedCalls(string sdate, string siteid) //100%
        {
            //HCSEntities ent = new HCSEntities();
            //ent.Configuration.ProxyCreationEnabled = false;
            string BedCall = "0";
            string BedECall = "0";
            string BedRecall = "0";
            string BedElevate = "0";
            //int iSiteID = Convert.ToInt16(siteid);
            //if (siteid == "0") siteid = Sessions.getSession().site.ID.ToString();
            //int iSiteID = Convert.ToInt16(siteid);
            //if ((sdate == "") || (sdate == "undefined"))
            //{
            //    var now = DateTime.Now;
            //    var dtFrom = new DateTime(now.Year, now.Month, 1);
            //    var dtTo = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " 23:59:59");
            //    BedCall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 1 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedECall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 2 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedRecall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 3 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedElevate = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 4 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //}
            //else
            //{
            //    string[] sDT = sdate.Split('-');
            //    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
            //    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
            //    BedCall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 1 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedECall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 2 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedRecall = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 3 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //    BedElevate = ent.HCSDatas.Where(s => s.Site_ID == iSiteID && s.DeviceType_ID == 1 && s.RecordType_ID == 4 && s.CreateDate >= dtFrom && s.CreateDate <= dtTo).Count().ToString();
            //}



            int iTot = Convert.ToInt16(BedCall) + Convert.ToInt16(BedECall) + Convert.ToInt16(BedRecall) + Convert.ToInt16(BedElevate);
            string tmp = BedCall + "," + BedECall + "," + BedRecall + "," + BedElevate + "," + iTot.ToString();

            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            return Newtonsoft.Json.JsonConvert.SerializeObject(tmp, x);
        }

        private string getChartECallsDaily(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1"; //Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getChartECalls", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getChartECalls", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getChartCallNotDaily(string sdate, string siteid) //100%
        {
            try
            {
                if (siteid == "0") siteid = "1";// Sessions.getSession().site.ID.ToString();
                string connnection = ConfigurationManager.AppSettings["Databaseconnection"].ToString();
                DataSet ds;
                if ((sdate == "") || (sdate == "undefined"))
                {
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getChartCallNot", "", "", siteid);
                }
                else
                {
                    string[] sDT = sdate.Split('-');
                    DateTime dtFrom = Convert.ToDateTime(sDT[0].ToString() + " 00:00:00");
                    DateTime dtTo = Convert.ToDateTime(sDT[1].ToString() + " 23:59:59");
                    ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "getChartCallNot", dtFrom, dtTo, siteid);
                }

                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);    //----------------- if you doing this for telerik grind and you want specific colums
                string daresult = DataSetToJSON(ds);                                    //----------------- get array data
                return daresult;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        #endregion

        #region extra

        public string DataSetToJSON(DataSet ds)
        {

            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (DataTable dt in ds.Tables)
            {
                object[] arr = new object[dt.Rows.Count + 1];

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    arr[i] = dt.Rows[i].ItemArray;
                }

                dict.Add(dt.TableName, arr);
            }

            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }

        private string getdbdata()
        {
            try
            {

                int mSiteID = 2;
                int mStockList_ID = 1;
                string connnection = "Data Source=154.0.162.20;Initial Catalog=BEMS;User ID=BEMS;Password=BEMS123456;";
                DataSet ds = Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset(connnection, "StockListSP", mSiteID, mStockList_ID);
                Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
                x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                string strings = Newtonsoft.Json.JsonConvert.SerializeObject(ds, x);
                string daresult = DataSetToJSON(ds);
                return strings;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return ex.ToString();
            }
        }

        private string getAllEq()
        {

            string[,] array = new string[,]
            {
                {"DEC", "123"}
            };


            DataTable table = new DataTable();
            table.Columns.Add("Month", typeof(string));
            table.Columns.Add("myval", typeof(string));

            // Here we add five DataRows.
            table.Rows.Add("DEC", "123");
            table.Rows.Add("JAN", "345");
            table.Rows.Add("FEB", "678");
            table.Rows.Add("MAR", "891");
            table.Rows.Add("APR", "345");


            Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
            x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            string strings = Newtonsoft.Json.JsonConvert.SerializeObject(table, x);
            return strings;
        }

        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}



//private string GetTableData()
//{
//    // Those parameters are sent by the plugin
//    var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
//    var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
//    var iSortCol = int.Parse(context.Request["iSortCol_0"]);
//    var iSortDir = context.Request["sSortDir_0"];


//    BEMSEntities ent = new BEMSEntities();
//    ent.Configuration.ProxyCreationEnabled = false;
//    var tmp = ent.Products.OrderBy(p => p.Name).ToList();
//}
//BEMSEntities ent = new BEMSEntities();
//ent.Configuration.ProxyCreationEnabled = false;
//var tmp = ent.Products.OrderBy(p => p.Name).ToList();
//Newtonsoft.Json.JsonSerializerSettings x = new Newtonsoft.Json.JsonSerializerSettings();
//x.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
//var records = Newtonsoft.Json.JsonConvert.SerializeObject(tmp, x);
//return records;
////if (records == null)
////{
////    return string.Empty;
////}
////var sb = new StringBuilder();
////var pagedResults = ent.Products.OrderBy(p => p.Name).ToList();
////var hasMoreRecords = false;
////sb.Append("\"aaData\": [");
////foreach (var result in pagedResults)
////{
////    if (hasMoreRecords)
////    {
////        sb.Append(",");
////    }

////    sb.Append("[");
////    sb.Append("\"" + result.Name + "\",");
////    sb.Append("]");
////    hasMoreRecords = true;
////}
////sb.Append("]}");
////return sb.ToString();
