﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dash.aspx.cs" Inherits="HVAC.Dash" %>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Nurse Call Data | Grid Data Page</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="csdtecmo.co.za" name="description"/>
<meta content="csdtecmo.co.za" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content">
<form id="form1" runat="server">
    
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="index.aspx">
			<img src="assets/admin/layout/img/logo.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
			<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="username username-hide-on-mobile"></span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="login.aspx">
							<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
                <br />
                                <!-- BEGIN SIDEBAR MENU -->
				<li class="start active open">
					<a href="javascript:;">
					<i class="icon-home"></i>
					<span class="title">NCS Menu</span>
					<span class="selected"></span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub-menu">
						<li class="active">
							<a href="index.aspx">
							<i class="icon-bar-chart"></i>
							Dashboard (Home page)</a>
						</li>
						<li>
							<a href="NurseCallData.aspx">
							<i class="icon-bulb"></i>
							Nurse Call Data (Grid)</a>
						</li>
						<li>
							<a href="NurseCallChart.aspx">
							<i class="icon-graph"></i>
							Nurse Call Data (Chart)</a>
						</li>
                        <li>
							<a href="NurseCallReport.aspx">
							<i class="icon-graph"></i>
							Nurse Call Reports</a>
						</li>
                         <li>
							<a href="NurseCallOperations.aspx">
							<i class="icon-graph"></i>
							Nurse Call Operations</a>
						</li>  
                       <%-- <li>
                            <a href="Favourites.aspx">
							<i class="icon-graph"></i>
							Favourites</a>
                        </li>--%>
					</ul>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			<div class="theme-panel hidden-xs hidden-sm">
				<%--<div class="toggler">
				</div>--%>
				<div class="toggler-close">
				</div>
				<div class="theme-options">
					<div class="theme-option theme-colors clearfix">
						<span>
						THEME COLOR </span>
						<ul>
							<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">
							</li>
							<li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">
							</li>
							<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
							</li>
							<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
							</li>
							<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
							</li>
							<li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">
							</li>
						</ul>
					</div>
					<div class="theme-option">
						<span>
						Theme Style </span>
						<select class="layout-style-option form-control input-sm">
							<option value="square" selected="selected">Square corners</option>
							<option value="rounded">Rounded corners</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Layout </span>
						<select class="layout-option form-control input-sm">
							<option value="fluid" selected="selected">Fluid</option>
							<option value="boxed">Boxed</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Header </span>
						<select class="page-header-option form-control input-sm">
							<option value="fixed" selected="selected">Fixed</option>
							<option value="default">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Top Menu Dropdown</span>
						<select class="page-header-top-dropdown-style-option form-control input-sm">
							<option value="light" selected="selected">Light</option>
							<option value="dark">Dark</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Mode</span>
						<select class="sidebar-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Menu </span>
						<select class="sidebar-menu-option form-control input-sm">
							<option value="accordion" selected="selected">Accordion</option>
							<option value="hover">Hover</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Style </span>
						<select class="sidebar-style-option form-control input-sm">
							<option value="default" selected="selected">Default</option>
							<option value="light">Light</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Sidebar Position </span>
						<select class="sidebar-pos-option form-control input-sm">
							<option value="left" selected="selected">Left</option>
							<option value="right">Right</option>
						</select>
					</div>
					<div class="theme-option">
						<span>
						Footer </span>
						<select class="page-footer-option form-control input-sm">
							<option value="fixed">Fixed</option>
							<option value="default" selected="selected">Default</option>
						</select>
					</div>
				</div>
			</div>
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Nurse Call Data <small>grid data</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.aspx">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="NurseCallData.aspx">Nurse Call Data</a>
						<i class="fa fa-angle-right"></i>
					</li>					
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
            <div class="row" id="sortable_portlets">
				<div class="col-md-5 column sortable">
                    <div class="portlet portlet-sortable box green-haze">
						        <div class="portlet-title">
							        <div class="caption">
								        <i class="fa fa-gift"></i>Date Ranges
							        </div>
							       <div class="tools">
								        <a href="javascript:;" class="collapse">
								        </a>
							        </div>
						        </div>
						        <div class="portlet-body">
							      	<div class="row">
				                        <div class="col-md-12">
                                            <!-- xxxPage-->
                                             <div id="dashboard-report-range" class="pull-left tooltips btn btn-sm btn-default" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">Select Date: | 
						                            <i class="icon-calendar">></i>&nbsp; <span class="thin uppercase visible-lg-inline-block" id="mdate"></span>&nbsp; <i class="fa fa-angle-down"></i>
					                         </div>
				                        </div>
			                        </div>
                                     <br />
                                     <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <button class="btn btn-success" id="Refreshall"><i class="icon-settings" ></i> Refresh</button>							
								    </div>
						       </div>
			        </div>
                </div>
            </div>

            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Call Elevated step to Emergency Call
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" data-load="true" data-url="" class="reload" onclick="getNC_CallElevateEmergencyCancel()">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
							</div>
                            <div class="actions">
                                 <%--<asp:Button ID="Button1" runat="server" Text="Export" class="btn btn-default btn-sm" OnClick="Button1_Click"/>--%>
                                <a class="btn btn-default btn-sm"  onclick="exportClicked()">Export</a>
                            </div>
						</div>
						<div class="portlet-body portlet-empty">

                            
						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>
			</div>

            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Call Not Attended step to Emergency Call
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" data-load="true" data-url="" class="reload" onclick="getNC_CallNotAttentEmergencyCancel()">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
							</div>
                            <div class="actions">
                                <a class="btn btn-default btn-sm"  onclick="exportClicked()">Export</a>
                            </div>
						   
						</div>
						<div class="portlet-body portlet-empty">
                            
						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>
			</div>

            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Call Start and End
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" data-load="true" data-url="" class="reload" onclick="getNC_StartEndCall()">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
							</div>
                            <div class="actions">
                                <a class="btn btn-default btn-sm"  onclick="exportClicked()">Export</a>
                            </div>
						</div>
						<div class="portlet-body portlet-empty">
                           
						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>
			</div>

                        <div class="row">
				<div class="col-md-12">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Emergency Call Start/ End 
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" data-load="true" data-url="" class="reload" onclick="getNC_EmergencyStartEndCall()">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
							</div>
                            <div class="actions">
                                <a class="btn btn-default btn-sm"  onclick="exportClicked()">Export</a>
                            </div>
						</div>
						<div class="portlet-body portlet-empty"> 
                               
						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>
			</div>
       
		
			</div>


			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 2015 &copy; Nurse call system <a href="www.csdtecmo.co.za" title="CSDTECMO WEBSITE" target="_blank">developed and maintained by CSDTECMO</a>
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="assets/global/scripts/cusGeneral.js" type="text/javascript"></script>

<script>
    jQuery(document).ready(function () {
        Metronic.blockUI();
        Index.initDashboardDaterange();
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        window.setTimeout(function () {
            Metronic.unblockUI();
        }, 3000);

       // $('#dashboard-report-range').daterangepicker({
       //     opens: (Metronic.isRTL() ? 'right' : 'left'),
       //     //startDate: moment().subtract('days', 7),
       //     //endDate: moment(),
       //     minDate: '01/01/2012',
       //     maxDate: '12/31/2099',
       //     dateLimit: {
       //         days: 365
       //     },
       //     showDropdowns: false,
       //     showWeekNumbers: true,
       //     timePicker: false,
       //     timePickerIncrement: 1,
       //     timePicker12Hour: true,
       //     alwaysShowCalendars: false,
       //     ranges: {
       //         'Today': [moment(), moment()],
       //         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
       //         'Last 7 Days': [moment().subtract('days', 6), moment()],
       //         'This Month': [moment().startOf('month'), moment().endOf('month')]
       //     }//,
       //     //buttonClasses: ['btn btn-sm'],
       //     //applyClass: ' blue',
       //     //cancelClass: 'default',
       //     //format: 'MM/DD/YYYY',
       //     //separator: ' to ',
       //     //locale: {
       //     //    applyLabel: 'Apply',
       //         //fromLabel: 'From',
       //         //toLabel: 'To',
       //         //customRangeLabel: 'Custom Range',
       //         //daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
       //         //monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
       //         //firstDay: 1
       //     //}
       // },
       //         function (start, end) {
       //             $('#dashboard-report-range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));                    
       //         }

        

       //);
        

    });
    
    //function onRequestStart(sender, args)
    //{
    //    alert("yes");
    //    if (args.get_eventTarget().indexOf("Button") >= 0) {
    //        args.set_enableAjax(false);
    //    }
    //}

    function exportClicked(sender, args) {
        //getNC_CallElevateEmergencyCancel();
        var masterTable = 0;<%-- //$find("<%= RadGrid1.ClientID<%-- %>").g--%>et_masterTableView();--%>
        masterTable.rebind();
        //masterTable.rebind();
        //document.forms["form1"].submit();
        masterTable.fireCommand("ExportToExcel", "");

    }
   
    $(document).on("click", "#Refreshall", function (e) {
        Metronic.blockUI();
        getNC_CallElevateEmergencyCancel();
        getNC_CallNotAttentEmergencyCancel();
        getNC_StartEndCall();
        getNC_EmergencyStartEndCall();

        window.setTimeout(function () {
            Metronic.unblockUI();
        }, 3000);

     });


    function getNC_CallElevateEmergencyCancel() {
        var ssdate = $('#mdate').text();
        $.ajax({
            url: "Myhandler.ashx?action=getNC_CallElevateEmergencyCancel&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    for (var i = 0; i <= data.length - 1; i++) {
                        data[i].StartedDate = data[i].StartedDate.replace('T', ' ');
                        data[i].ElevatedDate = data[i].ElevatedDate.replace('T', ' ');
                        data[i].EmergencyAckDate = data[i].EmergencyAckDate.replace('T', ' ');
                    }
                    var tableview = 0; <%--//$find("<%= RadGrid1.ClientID %>").get_masterTableView();--%>
                    tableview.set_dataSource(data);
                    tableview.dataBind();
                 };
            },
            failure: function (response) {
                alert("Error load data");
            }
        });
    }

    function getNC_CallNotAttentEmergencyCancel() {
        var ssdate = $('#mdate').text();
        $.ajax({
            url: "Myhandler.ashx?action=getNC_CallNotAttentEmergencyCancel&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    for (var i = 0; i <= data.length - 1; i++) {
                        data[i].StartedDate = data[i].StartedDate.replace('T', ' ');
                        data[i].ElevatedDate = data[i].ElevatedDate.replace('T', ' ');
                        data[i].EmergencyAckDate = data[i].EmergencyAckDate.replace('T', ' ');
                    }
                    var tableview = 0; <%--//$find("<%= RadGrid2.ClientID %>").get_masterTableView();--%>
                    tableview.set_dataSource(data);
                    tableview.dataBind();
                };
            },
            failure: function (response) {
                alert("Error load data");
            }
        });
    }

    function getNC_StartEndCall() {
        var ssdate = $('#mdate').text();
        $.ajax({
            url: "Myhandler.ashx?action=getNC_StartEndCall&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    for (var i = 0; i <= data.length - 1; i++) {
                        data[i].StartDate = data[i].StartDate.replace('T', ' ');
                        data[i].EndDate = data[i].EndDate.replace('T', ' ');
                    }
                    var tableview =0; //<%-- $find("<%= RadGrid3.ClientID %>").get_masterTableView();--%>
                    tableview.set_dataSource(data);
                    tableview.dataBind();
                };
            },
            failure: function (response) {
                alert("Error load data");
            }
        });
    }

    function getNC_EmergencyStartEndCall() {
        var ssdate = $('#mdate').text();
        $.ajax({
            url: "Myhandler.ashx?action=getNC_EmergencyStartEndCall&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.length > 0) {
                    for (var i = 0; i <= data.length - 1; i++) {
                        data[i].StartDate = data[i].StartDate.replace('T', ' ');
                        data[i].EndDate = data[i].EndDate.replace('T', ' ');
                    }
                    var tableview = 0; <%--//$find("<%= RadGrid4.ClientID %>").get_masterTableView();--%>
                        tableview.set_dataSource(data);
                        tableview.dataBind();
                    };
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
        }

   </script>
<!-- END JAVASCRIPTS -->
</form>
</body>
<!-- END BODY -->
</html>
