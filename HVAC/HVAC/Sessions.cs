﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace HVAC
{
    public class Sessions
    {
        public string sessionDate = "";
        public string user;
        public string site;
        public DataAccessLayer datalayer = new DataAccessLayer();
        public int dist_ID;
        public string dist_name;

        public Sessions()
        {

        }

        public void destroy()
        {
            HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = null;

        }

        public static Sessions getSession(HttpContext c)
        {
            if (c.Session[c.Session.SessionID] == null)
            {
                c.Session[c.Session.SessionID] = new Sessions();
            }

            return (Sessions)c.Session[c.Session.SessionID];
        }

        public static Sessions getSession()
        {
            try
            {

                if (HttpContext.Current.Session[HttpContext.Current.Session.SessionID] == null)
                {
                    if (HttpContext.Current.Request.Path.ToUpper().Contains("LOGIN"))
                    {
                        HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = new Sessions();
                    }
                    else
                    {
                        HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                    }
                }

                return (Sessions)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
            }
            catch (System.Exception)
            {

                HttpContext.Current.Response.Redirect("~/Login.aspx", true);
                return null;

            }

        }
        //public string _Heading;
        //public string Heading { get { return _Heading; } set { _Heading = value; } }

        public void setSite(int siteID)
        {
            //HCSEntities ent = new HCSEntities();
            //ent.Configuration.ProxyCreationEnabled = false;

            //site = (ent.Sites.Where(s => s.ID == siteID).FirstOrDefault<Site>());
        }

        public void setDate(string sdate)
        {
            sessionDate = "";
            sessionDate = sdate;
        }
        public void setApplicationType(string stype)
        {
            HttpContext.Current.Session["ApplicationType"] = null;
            HttpContext.Current.Session["ApplicationType"] = stype;
        }

        public bool login(string un, string pw, out int StoreCount)
        {
            StoreCount = 1;
            return true;
            //try
            //{
            //    return true;

            //    HCSEntities ent = new HCSEntities();
            //    ent.Configuration.ProxyCreationEnabled = false;
            //    IEnumerable<User> user = ent.Users.Where(x => x.Name == un && x.Password == pw).ToList();

            //    if (user.Count<User>() > 0)
            //    {
            //        this.user = user.First<User>();
            //        object usercount = NATS.mSite.getSitesPerUserCount(Sessions.getSession().user.ID);
            //        StoreCount = Convert.ToInt16(usercount);

            //        if (Convert.ToInt16(usercount) > 0)
            //        {
            //            return true;
            //        }
            //        else
            //        {
            //            return false;
            //        }
            //    }
            //    else
            //    {
            //        StoreCount = 0;
            //        return false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    StoreCount = -1;
            //    return false;
            //}
        }

        public int getUserRole(string loggedInUser)
        {
            return 1;
            //HCSEntities dbContext = new HCSEntities();
            //dbContext.Configuration.ProxyCreationEnabled = false;
            //if (loggedInUser == null)
            //{
            //    HttpContext.Current.Response.Redirect("~/Login.aspx", true);
            //    return null;
            //}
            //return (from r in dbContext.Roles
            //        join u in dbContext.Users on r.User_Role equals u.User_Role
            //        where u.ID == loggedInUser.ID && u.Name == loggedInUser.Name
            //        select r).FirstOrDefault();
        }

    }
}