﻿var cusGeneral = function () {

    return {

        BuildRecentActivities: function () {
            var ssdate = $('#mdate').text();
            var layout = "";
            var theader = "<li><div class='col1'><div class='cont'><div class='cont-col1'><div class='label label-sm label-info'><i class='fa fa-check'></i></div></div><div class='cont-col2'><div class='desc'>";
            var tfooter = "</div></div></div></div></li>";

            $.ajax({
                url: "Myhandler.ashx?action=getactivitiesLatest&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    var date;
                    for (var i = 0; i <= response.Table.length - 1; i++) {
                        //var date = $.datepicker.formatDate('yy/mm/dd', new Date(response.Table[i].Date));
                        var date = response.Table[i].Date.replace('T', ' ');
                        layout += theader
                        layout += 'Date - ' + date + ', Device - ' + response.Table[i].Device + ', Message - ' + response.Table[i].Message
                        layout += tfooter
                    }
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });          
            document.getElementById('ulFeeds').innerHTML = layout;
        },

        BuildRecentTask: function () {
            var ssdate = $('#mdate').text();
            var layout = "";
            var theader = "<li><div class='col1'><div class='cont'><div class='cont-col1'><div class='label label-sm label-info'><i class='fa fa-check'></i></div></div><div class='cont-col2'><div class='desc'>";
            var tfooter = "</div></div></div></div></li>";

            $.ajax({
                url: "Myhandler.ashx?action=gettaskLatest&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    var date;
                    for (var i = 0; i <= response.Table.length - 1; i++) {
                        //var date = $.datepicker.formatDate('yy/mm/dd', new Date(response.Table[i].Date));
                        var date = response.Table[i].Date.replace('T', ' ');
                        layout += theader
                        layout += 'Date - ' + date + ', Device - ' + response.Table[i].Device + ', Message - ' + response.Table[i].Message
                        layout += tfooter
                    }
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            document.getElementById('ulfeedsHU').innerHTML = layout;
        },

        BuildtodaysActivities: function () {
            var ssdate = $('#mdate').text();
            var layout = "";
            var theader = "<span class='details'><span class='label label-sm label-icon label-danger'><i class='fa fa-bolt'></i></span>";
            var tfooter = "</span>";

            $.ajax({
                url: "Myhandler.ashx?action=getactivitiesToday&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    var date;
                    for (var i = 0; i <= response.Table.length - 1; i++) {
                        //var date = $.datepicker.formatDate('yy/mm/dd', new Date(response.Table[i].Date));
                        var date = response.Table[i].Date.replace('T', ' ');
                        layout += theader
                        layout += 'Date - ' + date + ', Device - ' + response.Table[i].Device + ', Message - ' + response.Table[i].Message
                        layout += tfooter
                    }
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            document.getElementById('ulfeedsHU').innerHTML = layout;
        },
 
        BuildtLoadNurseCallData: function () {
            try {
                var view = $find("<%=  RadGrid1.ClientID %>").get_masterTableView();
                view.set_dataSource([]);
                view.dataBind();
            } catch (e) { }

            var ssdate = $('#mdate').text();
            $.ajax({
                url: "Myhandler.ashx?action=getNC_CallElevateEmergencyCancel&sdate=" + ssdate + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.length > 0) {
                        var xxx = $find("<%= RadGrid1.ClientID %>").get_masterTableView();
                        xxx.set_dataSource(data);
                        xxx.dataBind();
                    };
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
        }

    };

}();
