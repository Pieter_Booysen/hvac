﻿var cusChart = function () {

    return {
        
        initgetNC_CallNotAttentEmergencyCancel_Linechart: function () {
                if (!jQuery.plot) {
                    return;
                }

                function showChartTooltip(x, y, xValue, yValue) {
                    $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y - 40,
                        left: x - 40,
                        border: '0px solid #ccc',
                        padding: '2px 6px',
                        'background-color': '#fff'
                    }).appendTo("body").fadeIn(200);
                }
             
                var data = [];
                var totalPoints = 2000;
                var ssdate = //$('#mdate').text();
                var visitors2 = [];
            var siteId = 1;//("<%=Sessions.getSession().Site.ID%>");
                $("#ChartTitle").text(" CALL NOT ATTEND --> EMERGENCY CALL --> END/CANCEL CALL : " + ssdate);
                $.ajax({
                    url: "Myhandler.ashx?action=getNC_CallNotAttentEmergencyCancel_Linechart&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        visitors2 = response.Table;
                    },
                    failure: function (response) {
                        alert("Error load data");
                    }
                });
                ///Must do ajax call here
                if ($('#site_statistics').size() != 0) {

                    $('#site_statistics_loading').hide();
                    $('#site_statistics_content').show();

                    var plot_statistics = $.plot($("#site_statistics"),
                        [{
                            data: visitors2,
                            lines: {
                                fill: 0.6,
                                lineWidth: 0
                            },
                            color: ['#9f9ff8']
                        }, {
                            data: visitors2,
                            points: {
                                show: true,
                                fill: true,
                                radius: 5,
                                fillColor: " #9f9ff8",
                                lineWidth: 3
                            },
                            color: '#fff',
                            shadowSize: 0
                        }],

                        {
                            xaxis: {
                                tickLength: 0,
                                tickDecimals: 0,
                                mode: "categories",
                                min: 0,
                                font: {
                                    lineHeight: 14,
                                    style: "normal",
                                    variant: "small-caps",
                                    color: "#6F7B8A"
                                }
                            },
                            yaxis: {
                                ticks: 5,
                                tickDecimals: 0,
                                tickColor: "#eee",
                                font: {
                                    lineHeight: 14,
                                    style: "normal",
                                    variant: "small-caps",
                                    color: "#6F7B8A"
                                }
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            }
                        });

                    var previousPoint = null;
                    $("#site_statistics").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);

                                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Start to End Call activities');
                            }
                        } else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });

                    $("#site_activities").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint2 != item.dataIndex) {
                                previousPoint2 = item.dataIndex;
                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);
                                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                            }
                        }
                    });

                    $('#site_activities').bind("mouseleave", function () {
                        $("#tooltip").remove();
                    });
                }

            },

        initgetNC_CallElevateEmergencyCancel_Linechart: function () {
                if (!jQuery.plot) {
                    return;
                }

                function showChartTooltip(x, y, xValue, yValue) {
                    $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y - 40,
                        left: x - 40,
                        border: '0px solid #ccc',
                        padding: '2px 6px',
                        'background-color': '#fff'
                    }).appendTo("body").fadeIn(200);
                }

                var data = [];
                var totalPoints = 2000;
                var ssdate = $('#mdate').text();
                var visitors2 = [];
                var siteId = ("<%=Sessions.getSession().Site.ID%>");
                $("#ChartTitle").text(" ELEVATED CALL --> EMERGENCY CALL --> END CALL : " + ssdate);
                $.ajax({
                    url: "Myhandler.ashx?action=getNC_CallElevateEmergencyCancel_Linechart&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        visitors2 = response.Table;
                    },
                    failure: function (response) {
                        alert("Error load data");
                    }
                });
                ///Must do ajax call here
                if ($('#site_statistics').size() != 0) {

                    $('#site_statistics_loading').hide();
                    $('#site_statistics_content').show();

                    var plot_statistics = $.plot($("#site_statistics"),
                        [{
                            data: visitors2,
                            lines: {
                                fill: 0.6,
                                lineWidth: 0
                            },
                            color: ['#9f9ff8']
                        }, {
                            data: visitors2,
                            points: {
                                show: true,
                                fill: true,
                                radius: 5,
                                fillColor: " #9f9ff8",
                                lineWidth: 3
                            },
                            color: '#fff',
                            shadowSize: 0
                        }],

                        {
                            xaxis: {
                                tickLength: 0,
                                tickDecimals: 0,
                                mode: "categories",
                                min: 0,
                                font: {
                                    lineHeight: 14,
                                    style: "normal",
                                    variant: "small-caps",
                                    color: "#6F7B8A"
                                }
                            },
                            yaxis: {
                                ticks: 5,
                                tickDecimals: 0,
                                tickColor: "#eee",
                                font: {
                                    lineHeight: 14,
                                    style: "normal",
                                    variant: "small-caps",
                                    color: "#6F7B8A"
                                }
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            }
                        });

                    var previousPoint = null;
                    $("#site_statistics").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint != item.dataIndex) {
                                previousPoint = item.dataIndex;

                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);

                                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Start to End Call activities');
                            }
                        } else {
                            $("#tooltip").remove();
                            previousPoint = null;
                        }
                    });

                    $("#site_activities").bind("plothover", function (event, pos, item) {
                        $("#x").text(pos.x.toFixed(2));
                        $("#y").text(pos.y.toFixed(2));
                        if (item) {
                            if (previousPoint2 != item.dataIndex) {
                                previousPoint2 = item.dataIndex;
                                $("#tooltip").remove();
                                var x = item.datapoint[0].toFixed(2),
                                    y = item.datapoint[1].toFixed(2);
                                showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                            }
                        }
                    });

                    $('#site_activities').bind("mouseleave", function () {
                        $("#tooltip").remove();
                    });
                }

            },

        initgetNC_EStartEndCall_Linechart: function () {


            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 2000;
            var ssdate = $('#mdate').text();
            var visitors2 = [];
            var siteId = ("<%=Sessions.getSession().Site.ID%>");
            $("#ChartTitle").text(" EMERGENCY CALL --> END CALL : " + ssdate);
            $.ajax({
                url: "Myhandler.ashx?action=getNC_EStartEndCall_Linechart&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    visitors2 = response.Table;
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            ///Must do ajax call here
            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"),
                    [{
                        data: visitors2,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#9f9ff8']
                    }, {
                        data: visitors2,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: " #9f9ff8",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Start to End Call activities');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });

                $("#site_activities").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }

        },

        initgetNC_StartEndCall_Linechart: function () {
            

            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 2000;
            var ssdate = $('#mdate').text();
            var visitors2 = [];
            var siteId = ("<%=Sessions.getSession().Site.ID%>");
            $("#ChartTitle").text(" START CALL --> END CALL : " + ssdate);
            $.ajax({
                url: "Myhandler.ashx?action=getNC_StartEndCall_Linechart&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    visitors2 = response.Table;
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            ///Must do ajax call here
            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"),
                    [{
                        data: visitors2,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#9f9ff8']
                    }, {
                        data: visitors2,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: " #9f9ff8",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Start to End Call activities');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });

                $("#site_activities").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }

        },

        initBarChartsFaultyHandHeld: function () {
            // bar chart:
            var data = GenerateSeries(0);
           
            function GenerateSeries(added) {
                var data = [];
                var ssdate = $('#mdate').text();
                var siteId = ("<%=Sessions.getSession().Site.ID%>");
                $.ajax({
                    url: "Myhandler.ashx?action=getchartFaultyhandheld&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                    contentType: "application/json; charset=utf-8",
                    type: 'Get',
                    dataType: "json",
                    jsonp: 'callback',
                    async: false,
                    success: function (response) {
                        for (i = 0; i <= response.Table.length - 1; i++) {
                            data.push([response.Table[i].Date, response.Table[i].Calls]);
                        }
                    },
                    failure: function (response) {
                        alert("Error load data");
                    }
                });
                return data;
            }
            var options = {
                series: {
                    bars: {
                        show: true
                    }
                },
                bars: {
                    barWidth: 0.8,
                    lineWidth: 0, // in pixels
                    shadowSize: 0,
                    align: 'left'
                },

                grid: {
                    tickColor: "#eee",
                    borderColor: "#eee",
                    borderWidth: 1
                }
            };

            if ($('#chartBar1').size() !== 0) {
                $.plot($("#chartBar1"), [{
                    data: data,
                    lines: {
                        lineWidth: 1,
                    },
                    shadowSize: 0
                }], options);
            }
        },

        initChartsEmergency: function () {
            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }
            var data = [];
            var totalPoints = 31;
            var ssdate = $('#mdate').text();
            var visitors2 = [];
            var siteId = ("<%=Sessions.getSession().Site.ID%>");
            
            $.ajax({
                url: "Myhandler.ashx?action=getchartECalls&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    visitors2 = response.Table;
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });

            ///Must do ajax call here
            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"),
                    [{
                        data: visitors2,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#f89f9f']
                    }, {
                        data: visitors2,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: "#f89f9f",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Emergency calls');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });


            }

            if ($('#site_activities').size() != 0) {
                //site activities
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                $('#site_activities_content').show();

                var data1 = [
                    ['DEC', 300],
                    ['JAN', 600],
                    ['FEB', 1100],
                    ['MAR', 1200],
                    ['APR', 860],
                    ['MAY', 1200],
                    ['JUN', 1450],
                    ['JUL', 1800],
                    ['AUG', 1200],
                    ['SEP', 600]
                ];


                var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: data1,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: data1,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: data1,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                $("#site_activities").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }





        },

        initChartsCallNot: function () {
            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 31;
            var ssdate = $('#mdate').text();
            var visitors2 = [];
            var siteId = ("<%=Sessions.getSession().Site.ID%>");
            $.ajax({
                url: "Myhandler.ashx?action=getchartCallNot&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    visitors2 = response.Table;
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });

            ///Must do ajax call here
            if ($('#site_statistics2').size() != 0) {

                $('#site_statistics_loading2').hide();
                $('#site_statistics_content2').show();

                var plot_statistics = $.plot($("#site_statistics2"),
                    [{
                        data: visitors2,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#9f9ff8']
                    }, {
                        data: visitors2,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: " #9f9ff8",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics2").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Calls not attented');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });

                $("#site_activities2").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities2').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }

        },

        initPieCharts: function () {
            var data = [];
            var ssdate = $('#mdate').text();
            var series = Math.floor(0) + 1;
            series = series < 4 ? 4 : series;
            var siteId = ("<%=Sessions.getSession().Site.ID%>");

            $.ajax({
                url: "Myhandler.ashx?action=getBedCalls&sdate=" + ssdate + "&siteid=" + 0 + "&trick=" + (new Date()).getMilliseconds(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    var arr = response.split(',');
                    data[0] = {
                        label: "Bed Calls" + (1),
                        data: arr[0]
                    };
                    data[1] = {
                        label: "Emergency Calls" + (2),
                        data: arr[1]
                    };
                    data[2] = {
                        label: "Recalls" + (3),
                        data: arr[2]
                    };
                    data[3] = {
                        label: "Elevated Calls" + (4),
                        data: arr[3]
                    };
                    $('#pieheader').text("monthly stats... " + arr[4] + "  Bed calls were made");
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            // GRAPH 1
            if ($('#pie_chart_1').size() !== 0) {
                $.plot($("#pie_chart_1"), data, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    legend: {
                        show: false
                    }
                });
            }

            function pieHover(event, pos, obj) {
                if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                $("#hover").html('<span style="font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
            }

            function pieClick(event, pos, obj) {
                if (!obj)
                    return;
                percent = parseFloat(obj.series.percent).toFixed(2);
                alert('' + obj.series.label + ': ' + percent + '%');
            }

        }
    };

}();

