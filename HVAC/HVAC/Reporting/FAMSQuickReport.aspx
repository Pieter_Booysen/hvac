﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAMSQuickReport.aspx.cs" Inherits="HVAC.FAMSQuickReport" %>
<!DOCTYPE html>
<html>
<head>
    <title>HVAC Reporting</title>
    <meta charset="utf-8" />

    <link rel="stylesheet" type="text/css" href="../assets/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="../assets/easyui/icon.css">
    <script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="../assets/easyui/datagrid-filter.js"></script>
    <script type="text/javascript" src="../assets/easyui/datagrid-export.js"></script>
    <script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-detailview.js"></script>
    <script type="text/javascript" src="../assets/js/date.js"></script>
    <script src="../assets/FAMS/FamsFunctions.js"></script>
    <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../assets/ToExcel/jquery.tableToExcel.js" type="text/javascript"></script>
    <script src="../assets/ToExcel/JsExport.js" type="text/javascript"></script>
</head>
<body>
    <!-- END PAGE HEADER-->
    <div class="easyui-panel" title="Report Parameters - Select Quick View Report from drop down list" style="width: 100%; max-width: 600px; padding: 10px 10px; margin-bottom: 20px">
        <div style="margin-bottom: 5px">
            <input id="startdate" name="daterange" class="easyui-datebox" labelwidth="200px" data-options="formatter:myformatter,parser:myparser" style="width: 400px" label="Begin date :">
        </div>
        <div style="margin-bottom: 5px">
            <input id="enddate" name="daterange" class="easyui-datebox" labelwidth="200px" data-options="formatter:myformatter,parser:myparser" style="width: 400px" label="End date :">
        </div>
        <div style="margin-bottom: 5px">
            <select id="FAMSReports" name="FAMSReports" class="easyui-combobox" value="Please select - Report" label="Quick Report:" labelwidth="200px" style="width: 400px">
                <option value="0">Please select - Report</option>
                <option value="1">Latest Sensor Info</option>
                <option value="2">Info per Sensor</option>
            </select>
        </div>
       
                <div style="padding: 5px 40px;float:left">
                    <a id="Load1" class="easyui-linkbutton" data-options="iconCls:'icon-reload'" style="width: 120px" onclick="buttonclick()">Reload</a>
                </div>
                <div style="padding: 5px 20px;float:left">
                    <a id="exportToCSV" class="easyui-linkbutton hide" data-options="iconCls:'icon-reload'" style="width: 120px" onclick="exportToCSV() ">Export To CSV</a>               
                </div>
        


    </div>
    <div id="dd"></div>
    <div style="margin-bottom: 20px">
        <table id="dg" style="width: 1050px; height: 550px" title="DataGrid - SubGrid"></table>
    </div>



    <script type="text/javascript">
        var weburl = "<%=ConfigurationManager.AppSettings["webApiURL"]%>";
        var weburl2 = "<%=ConfigurationManager.AppSettings["webApiURL2"]%>";

        var fd = Date.today().clearTime().moveToFirstDayOfMonth();
        var firstday = fd.toString("yyyy-MM-dd");
        var ld = Date.today().clearTime().moveToLastDayOfMonth();
        var lastday = ld.toString("yyyy-MM-dd");


        jQuery(document).ready(function () {
            $('#startdate').datebox('setValue', firstday);
            $('#enddate').datebox('setValue', lastday);
        });

        function ExportExcel() {
            //$('#dg').datagrid('getData');
            //$('#dg').tblToExcel();
              //var footers=$('#dg').datagrid('getFooterRows');
            //var sum2 = footers[0].Volume;
            //alert(sum2);
            //$('#dg').datagrid('toExcel', {
            //    filename: 'datagrid.xls',
            //    worksheet: 'Worksheet'
            //});

            //$('#ddv').datagrid('toExcel', {
            //    filename: 'datagrid.xls',
            //    worksheet: 'Worksheet'
            //});
            
        }


        function ExportExcel2() {

          

            var data = $('#dg').datagrid('getData');
            var rows = data.rows;
            var sum = 0;

            for (i = 0; i < rows.length; i++) {
                //sum += rows[i].Volume;
                sum+=parseFloat(rows[i].Volume);
            }

            $('#dg').datagrid('reloadFooter', [
                       {name: 'Volume', Product:'Total',Volume: sum,action:'n/a'}
            ]);
            alert(document.getElementById("ddv").id);
            var ddv2 = document.getElementById("ddv");
            ddv2.datagrid('reloadFooter', [
                       {name: 'Volume', Product:'Total',Volume: 123}
            ]);
          
        }

        function myformatter(date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
        }
        function myparser(s) {
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0], 10);
            var m = parseInt(ss[1], 10);
            var d = parseInt(ss[2], 10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
                return new Date(y, m - 1, d);
            } else {
                return new Date();
            }
        }

        var iReportType = 0;
        var iReportLevel = 0;
        var iEID = 0;
        var iPID = 0;
        var iSID = 0;
        var iMID = 0;
        var iCID = 0;
        var iUID = 0;
        var iSiteID = 0;
        var sFromDate = "";
        var sToDate = "";
        function callBlock() {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            setTimeout($.unblockUI, 3000);
        }


        $('#FAMSReports').combobox({
            onSelect: function (row) {
                var target = this;
                setTimeout(function () {
                    if ($('#FAMSReports').combobox('getValue') == 'Please select - Report') {
                        return false;
                    }
                    loaddataGrid();
                }, 0);
            }
        })

        function buttonclick() {
            if ($('#FAMSReports').combobox('getValue') == '0') {
                alert("Please select a report from list!!!")
                return false;
            }
            else {
                loaddataGrid();
            }
        }

        var tsum = 0;
        function loaddataGrid() {
            var temp = "";
            if ($('#FAMSReports').combobox('getValue') == '0') {
                return false;
            }
            sFromDate = $('#startdate').datebox('getValue');	// get datebox value
            sToDate = $('#enddate').datebox('getValue');	// get datebox value
            callBlock();
           
            if ($('#FAMSReports').combobox('getValue') == '1') {
                //var p = $('#dg').datagrid('getPanel');
                //p.panel('destroy');
               // $('#dg').datagrid('clear');
               // $('#dg').datagrid('reload');  
                //$('#dg').datagrid('loadData', {"total":0,"rows":[]});
                $('#dg').datagrid({
                    //url: weburl + 'queryType=SqlSp&sp=QV_GetTransfersPerEquipment&paramlist=' + 1 + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds(),
                    url: weburl + "queryType=SqlSp&sp=QV_GetLatestInfo3&paramlist=" + '1' + "&trick=" + (new Date()).getMilliseconds(),
                    contentType: "application/json", 
                    showFooter: true,
                    singleSelect: true,
                    rownumbers: true,
                    loadMsg: 'Busy loading',
                    height: 'auto',
                    //width: 'auto',
                    columns: [[

                        { field: 'Site', title: 'Site', width: 80 },
                        { field: 'SiteDesc', title: 'SiteDesc', width: 200 },
                        { field: 'Station', title: 'Station', width: 80 },
                        { field: 'Sensor', title: 'Sensor', width: 100 },
                         {
                                    field: 'LatestDate', title: 'LatestDate', width: 120,
                                    formatter: function (value, row, index) {
                                        return value.replace("T", " ");
                                    }
                                },
                        { field: 'MeasureInfo', title: 'MeasureInfo', width: 100 },
                        { field: 'SensorUse', title: 'SensorUse', width: 150 },                      
                         { field: 'EngUnit', title: 'EngUnit', width: 100 },
                        { field: 'Reading', title: 'Reading', width: 80 }
                    ]],
                    onLoadError: function (data) {
                        alert("Error loading");
                    },
               

                })
                $('#dg').datagrid('reload');
                return false;
            }
            //STOCK RECIEVED
            else if ($('#FAMSReports').combobox('getValue') == '2') {
                iReportType = 2;
                $('#dg').datagrid({
                    url: weburl + 'queryType=SqlSp&sp=QV_GetSensorInfo&paramlist=' + 1 + "&trick=" + (new Date()).getMilliseconds(),
                    showFooter: true,
                    singleSelect: true,
                    rownumbers: true,
                    loadMsg: 'Busy loading',
                    view: detailview,
                    columns: [[
                        { field: 'Station', title: 'Station', width: 300, editor: 'text' },
                        { field: 'SensorName', title: 'SensorName', width: 300, editor: 'text' }
                    ]],
                    detailFormatter: function (index, row) {
                        return '<div style="padding:2px;position:relative;"><table ID="Tbl1" class="ddv" style="width:1000px;height:250px"></table></div>';
                    },
                    onExpandRow: function (index, row) {
                        callBlock();
                        var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
                        iEID = row.SenID;
                        iPID = row.SID;
                        ddv.datagrid({
                            url: weburl + 'queryType=SqlSp&sp=QV_GetSensorInfoDetail&paramlist=' + row.SID + ',' + row.SenID + "&trick=" + (new Date()).getMilliseconds(),

                            showFooter: true,
                            singleSelect: true,
                            rownumbers: true,
                            loadMsg: 'Busy loading',
                            //height: 'auto',
                            columns: [[
                                { field: 'Site', title: 'Site', width: 100 },
                                { field: 'SiteDesc', title: 'SiteDesc', width: 100 },
                                { field: 'Station', title: 'Station', width: 100 },
                                { field: 'Sensor', title: 'Sensor', width: 100 },
                                 {
                                    field: 'Createdate', title: 'Createdate', width: 120,
                                    formatter: function (value, row, index) {
                                        return value.replace("T", " ");
                                    }
                                },
                                { field: 'SensorUse', title: 'SensorUse', width: 100 },
                                { field: 'EngUnit', title: 'EngUnit', width: 100 },
                                { field: 'Reading', title: 'Reading', width: 100 }
                                
                            ]],
                            onResize: function () {
                                $('#dg').datagrid('fixDetailRowHeight', index);
                            },
                            onLoadSuccess: function () {
                                setTimeout(function () {
                                    $('#dg').datagrid('fixDetailRowHeight', index);
                                }, 0);
                            }
                        });
                        $('#dg').datagrid('fixDetailRowHeight', index);
                    }
                });
            }
            //useage per site ----------------------------------------------------------------------
            else if ($('#FAMSReports').combobox('getValue') == '10') {
                iReportType = 10;

                $('#dg').datagrid({
                    url: weburl + 'queryType=SqlSp&sp=QV_GetUsagePerSite&paramlist=' + 1 + ',' + sessionStorage.getItem("User_ID") + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds(),
                    showFooter: true,
                    singleSelect: true,
                    rownumbers: true,
                    loadMsg: 'Busy loading',
                    view: detailview,
                    columns: [[

                        { field: 'Name', title: 'Site Name', width: 400, editor: 'text' },
                        { field: 'Volume', title: 'Total Volume dispensed on site', width: 400, editor: 'text' },
                        {
                            field: 'action', title: 'Expand', width: 200, align: 'center',
                            formatter: function (value, row, index) {
                                var e = '<a href="javascript:void(0)" onclick="getSelected(' + row.SiteID + ',' + 1 + ')">click</a> ';
                                return e;
                            }
                        }
                    ]],
                    detailFormatter: function (index, row) {
                        return '<div style="padding:2px;position:relative;"><table ID="Tbl1" class="ddv" style="width:1000px;height:250px"></table></div>';
                    },
                    onExpandRow: function (index, row) {
                        callBlock();
                        var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
                        iSiteID = row.SiteID;
                        ddv.datagrid({
                            url: weburl + 'queryType=SqlSp&sp=QV_GetUsagePerSite_Detail_Level1&paramlist=' + 1 + ',' + sessionStorage.getItem("User_ID") + ',' + row.SiteID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds(),
                            view: detailview,
                             showFooter: true,
                            singleSelect: true,
                            rownumbers: true,
                            loadMsg: 'Busy loading',
                            //height: 'auto',
                            columns: [[
                                { field: 'Store', title: 'Store', width: 250 },
                                { field: 'Product', title: 'Product', width: 250 },

                                { field: 'Volume', title: 'Volume', width: 250 },
                                {
                                    field: 'action', title: 'Expand', width: 100, align: 'center',
                                    formatter: function (value, row, index) {
                                        var e = '<a href="javascript:void(0)" onclick="getSelected_Multi(' + row.SID + ',' + row.PID + ',' + 2 + ')">click</a> ';
                                        return e;
                                    }
                                }
                            ]],
                            detailFormatter: function (index, row) {
                                return '<div style="padding:2px;position:relative;"><table ID="Tbl1" class="dddv" style="width:900px;height:250px"></table></div>';
                            },
                            onExpandRow: function (index, row2) {
                                callBlock();
                                var dddv = $(this).datagrid('getRowDetail', index).find('table.dddv');
                                dddv.datagrid({
                                    url: weburl + 'queryType=SqlSp&sp=QV_GetUsagePerStore_Detail&paramlist=' + 1 + ',' + row2.PID + ',' + row2.SID + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds(),

                                    showFooter: true,
                                    singleSelect: true,
                                    rownumbers: true,
                                    loadMsg: 'Busy loading',

                                    columns: [[
                                        { field: 'Store', title: 'Store', width: 100 },
                                        { field: 'Registration', title: 'Registration', width: 100 },
                                        {
                                            field: 'Date', title: 'Date', width: 120,
                                            formatter: function (value, row, index) {
                                                return value.replace("T", " ");
                                            }
                                        },
                                        { field: 'Measurement', title: 'Measurement', width: 80 },
                                        { field: 'KMHour', title: 'KM/Hour', width: 80 },
                                        { field: 'Volume', title: 'Volume', width: 80 },
                                        { field: 'Operator', title: 'Operator', width: 80 },
                                        { field: 'Driver', title: 'Driver', width: 80 },
                                        { field: 'Authorization', title: 'Authorization', width: 80 }

                                    ]],
                                    onResize: function () {
                                        $('#ddv').datagrid('fixDetailRowHeight', index);
                                    },
                                    onLoadSuccess: function () {
                                        setTimeout(function () {
                                            $('#ddv').datagrid('fixDetailRowHeight', index);
                                        }, 0);
                                    },
                                    onResize: function () {
                                        $('#ddv').datagrid('fixDetailRowHeight', index);
                                    },
                                    onLoadSuccess: function () {
                                        setTimeout(function () {
                                            $('#ddv').datagrid('fixDetailRowHeight', index);
                                        }, 0);
                                    }
                                });
                                $('#ddv').datagrid('fixDetailRowHeight', index);
                            },
                            onResize: function () {
                                $('#dg').datagrid('fixDetailRowHeight', index);
                            },
                            onLoadSuccess: function () {
                                setTimeout(function () {
                                    $('#dg').datagrid('fixDetailRowHeight', index);
                                }, 0);
                            }
                        });
                        $('#dg').datagrid('fixDetailRowHeight', index);
                    }
                });
                //}
            }
            else if ($('#FAMSReports').combobox('getValue') == '11') {
                iReportType = 11;

                $('#dg').datagrid({
                    url: weburl + 'queryType=SqlSp&sp=QV_GetUsageRecievingManual&paramlist=' + 1 + ',' + sFromDate + ',' + sToDate + "&trick=" + (new Date()).getMilliseconds(),
                    showFooter: true,
                    singleSelect: true,
                    rownumbers: true,
                    loadMsg: 'Busy loading',
                    columns: [[

                        { field: 'StoreName', title: 'Store Name', width: 100, editor: 'text' },
                        { field: 'TankName', title: 'Tank Name', width: 100, editor: 'text' },
                        { field: 'ProductName', title: 'Product Name', width: 100, editor: 'text' },
                        {
                            field: 'CreateDate', title: 'Date Recieved', width: 120,
                            formatter: function (value, row, index) {
                                return value.replace("T", " ");
                            }
                        },
                        { field: 'Volume', title: 'Volume', width: 100, editor: 'text' }
                    ]]


                });
                //}
            }
        }

        function getSelected_Multi(varb1, varb2, varb3) {

            if (varb3 = 2) {

                iReportLevel = 2;
                iSID = varb1;
                iPID = varb2;
            }


            $('#dd').dialog({
                title: 'Expansion view',
                width: 1000,
                height: 600,
                closed: false,
                cache: false,
                href: 'QV_Preview.aspx?trick=' + new Date().getMilliseconds(),
                modal: true
            });
        }

        function getSelected(varb1, varb2) {
            if ($('#FAMSReports').combobox('getValue') == '1') {
                iReportType = 1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '2') {
                iReportType = 2;
                iSID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '3') {
                iReportType = 3;
                iEID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '4') {
                iReportType = 4;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '5') {
                iReportType = 5;
                iMID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '6') {
                iReportType = 6;
                iCID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '7') {
                iReportType = 7;
                iEID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '8') {
                iReportType = 8;
                alert('Future');
                return false;
            }
            else if ($('#FAMSReports').combobox('getValue') == '9') {
                iReportType = 9;
                iEID = varb1;
                iPID = varb2;
            }
            else if ($('#FAMSReports').combobox('getValue') == '10') {
                iReportType = 10;
                iSiteID = varb1;
                iReportLevel = 1;
            }
            else if ($('#FAMSReports').combobox('getValue') == '11') {
                iReportType = 11;
            }
            $('#dd').dialog({
                title: 'Expansion view',
                width: 1000,
                height: 600,
                closed: false,
                cache: false,
                href: 'QV_Preview.aspx?trick=' + new Date().getMilliseconds(),
                modal: true
            });
        }
        function exportToCSV() {
             //<option value="0">Please select - Report</option>
             //   <option value="1">Overview</option>
             //   <option value="2">Usage per Store</option>
             //   <option value="3">Usage per Equipment</option>
             //   <option value="4">Usage per Allocation</option>
             //   <option value="5">Usage per Master Equipment</option>
             //   <option value="6">Usage per CostCentre</option>
             //   <option value="7">Fuel Transfers</option>
             //   <option value="8">KPI/SARS/Logbook</option>
             //   <option value="9">Stock Received (FAMS)</option>
             //   <option value="11">Stock Received (MANUAL)</option>
            //   <option value="10">Usage per Site</option>
        
            var sFromDateEx = $('#startdate').datebox('getValue');	// get datebox value
            var sToDateEx = $('#enddate').datebox('getValue');
            callBlock();
            if ($('#FAMSReports').combobox('getValue') == '0') {
                 $.messager.alert('Alert', 'Please select a report', function (r) { });
            }
             else if ($('#FAMSReports').combobox('getValue') == '1') {
                  //   <option value="2">Usage per Store</option>

                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerProduct&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_Product' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
            else if ($('#FAMSReports').combobox('getValue') == '2') {
                  //   <option value="2">Usage per Store</option>

                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerStore&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_Store' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
            else if ($('#FAMSReports').combobox('getValue') == '3') {
                //<option value="3">Usage per Equipment</option>
                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerEquipment&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_Equipment' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
            else if ($('#FAMSReports').combobox('getValue') == '4') {
                 //<option value="4">Usage per Allocation</option>

                 //var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerEquipment&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 //JSONToCSVConvertor(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_Equipment' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
            else if ($('#FAMSReports').combobox('getValue') == '5') {
                 //   <option value="5">Usage per Master Equipment</option>

                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerMasterEquipment&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_MasterEquipment' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
             else if ($('#FAMSReports').combobox('getValue') == '6') {
                   //   <option value="6">Usage per CostCentre</option>

                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetUsagePerCostCentre&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_CostCentre' + ' ' + sFromDateEx + '-' + sToDateEx);
            }
             else if ($('#FAMSReports').combobox('getValue') == '7') {
                   //   <option value="7">Fuel Transfers</option>

                 var urlExcel = weburl + 'queryType=SqlSp&sp=Excel_QV_GetFuelTransfers&paramList=' + 1 + ',' + sFromDateEx + ',' + sToDateEx;
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_FuelTransfers' + ' ' + sFromDateEx + '-' + sToDateEx);
            }  
             else if ($('#FAMSReports').combobox('getValue') == '8') {
                   //   <option value="8">KPI/SARS/Logbook</option>

                var urlExcel = weburl + "queryType=SqlSp&sp=Excel_QV_GetKPILogbook&paramlist=" + 1 + ',' + sFromDateEx + ',' + sToDateEx + "&trick=" + (new Date()).getMilliseconds();
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_KPILogbook' + ' ' + sFromDateEx + '-' + sToDateEx);
            }  
            else if ($('#FAMSReports').combobox('getValue') == '9') {
                  //   <option value="9">Stock Received (FAMS)</option>

                var urlExcel = weburl + "queryType=SqlSp&sp=Excel_QV_StockRecieved&paramlist=" + 1 + ',' + sFromDateEx + ',' + sToDateEx + "&trick=" + (new Date()).getMilliseconds();
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_StockRecieved' + ' ' + sFromDateEx + '-' + sToDateEx);
            }  
             else if ($('#FAMSReports').combobox('getValue') == '10') {
                  //    <option value="10">Usage per Site</option>

                var urlExcel = weburl + "queryType=SqlSp&sp=Excel_QV_UsagePerSite&paramlist=" + 1 + ',' + sFromDateEx + ',' + sToDateEx + "&trick=" + (new Date()).getMilliseconds();
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_Usage_Per_Site' + ' ' + sFromDateEx + '-' + sToDateEx);
            }  
            else if ($('#FAMSReports').combobox('getValue') == '11') {
                  //    <option value="11">Stock Received (MANUAL)</option>

                var urlExcel = weburl + "queryType=SqlSp&sp=Excel_QV_GetRecieving_Manual&paramlist=" + 1 + ',' + sFromDateEx + ',' + sToDateEx + "&trick=" + (new Date()).getMilliseconds();
                 JSONToCSVConvertorForCalculation(urlExcel, sessionStorage.getItem("Account_Name") + '_Report_StockRecieved_Manual' + ' ' + sFromDateEx + '-' + sToDateEx);
            }  
           
            
        }
    </script>

</body>
</html>
