﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="HVAC.Login" %>

<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>HVAC | Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="FAMS Portal" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="res/img/logo.png" />
    <%-- <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>--%>


    <link rel="stylesheet" type="text/css" href="assets/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="assets/easyui/icon.css">
    <%-- <script type="text/javascript" src="assets/easyui/jquery.easyui.min.js"></script>--%>
    <link href="res/css/hvac.css" rel="stylesheet" />
</head>
<!-- END HEAD -->
<body class=" login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="margin: 5px 5px 5px">
        <a href="index.html">
            <img src="res/img/logo.png" style="height: 200px;" />
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form id="loginform" class="login-form visable" action="" method="" runat="server">

            <h3 class="form-title">Login to your account</h3>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span>Enter any username and password. </span>
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" id="username" runat="server" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" runat="server" />
                </div>
            </div>
            <div class="form-actions">
                <%-- <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1" />
                    Remember me
                    <span></span>
                </label>--%>
                <a id="login" class="btn hvac pull-right" data-options="iconCls:'icon-reload'" style="width: 100px" onclick="FAMSLogin()">Login</a>
            </div>

        </form>

        <form id="accountselector" class="login-form display-none" action="" method="">
            <div class="portlet-body">
                <div class="mt-element-list">
                    <div class="mt-list-head list-simple font-white bg-blue">
                        <div class="list-head-title-container">
                            <h3 class="list-title" style="padding-right: 0px">Please select a site</h3>
                        </div>
                    </div>
                    <div class="mt-list-container list-simple ext-1" style="background-color: #fff !important">
                        <ul id='accountlist' style="overflow: hidden; overflow-y: scroll; height: auto; max-height: 45vh">
                        </ul>
                    </div>
                </div>
            </div>
        </form>

        <!--   <div class="login-options">
                <h4>Or login with</h4>
                <ul class="social-icons">
                    <li>
                        <a class="facebook" data-original-title="facebook" href="javascript:;"> </a>
                    </li>
                    <li>
                        <a class="twitter" data-original-title="Twitter" href="javascript:;"> </a>
                    </li>
                    <li>
                        <a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a>
                    </li>
                    <li>
                        <a class="linkedin" data-original-title="Linkedin" href="javascript:;"> </a>
                    </li>
                </ul>
            </div>--%>
            <%--   <div class="forget-password">
                <h4>Forgot your password ?</h4>
                <p>
                    no worries, click
                    <a href="javascript:;" id="forget-password"> here </a> to reset your password.
                </p>
            </div>--%>
            <%--             <div class="create-account">
                <p>
                    Don't have an account yet ?&nbsp;
                    <a href="javascript:;" id="register-btn"> Create an account </a>
                </p>
            </div>--%>
        </form>
        <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <!--<form class="forget-form" action="index.html" method="post">
            <h3>Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>
            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
                </div>
            </div>
            <div class="form-actions">
                ca
                <button type="button" id="back-btn" class="btn red btn-outline">Back </button>
                <button type="submit" class="btn green pull-right"> Submit </button>
            </div>
        -->
        <!-- END FORGOT PASSWORD FORM -->
        <!-- BEGIN REGISTRATION FORM -->
        <!-- END REGISTRATION FORM -->

    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">2018 &copy; Tecmo Automation. </div>
    <!-- END COPYRIGHT -->
    <!--[if lt IE 9]>
    <script src="../assets/global/plugins/respond.min.js"></script>
    <script src="../assets/global/plugins/excanvas.min.js"></script>
    <script src="../assets/global/plugins/ie8.fix.min.js"></script>
    <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <%--    <script src="/assets/pages/scripts/login-4.min.js" type="text/javascript"></script>--%>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <!-- END THEME LAYOUT SCRIPTS -->


    <script>
        var weburl = "<%=ConfigurationManager.AppSettings["webApiURL"]%>";
        var weburl2 = "<%=ConfigurationManager.AppSettings["webApiURL2"]%>";



        function callBlock() {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            setTimeout($.unblockUI, 3000);
        }

        //var input = document.getElementById("login");
        //input.addEventListener("keyup", function (event) {
        //    event.preventDefault();
        //    if (event.keyCode === 13) {
        //        FAMSLogin();
        //        //document.getElementById("login").click();
        //    }
        //});
        function clearListCookies() {
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) {
                var spcook = cookies[i].split("=");
                deleteCookie(spcook[0]);
            }
            function deleteCookie(cookiename) {
                var d = new Date();
                d.setDate(d.getDate() - 1);
                var expires = ";expires=" + d;
                var name = cookiename;
                //alert(name);
                var value = "";
                document.cookie = name + "=" + value + expires + "; path=/acc/html";
            }

        }
        document.querySelector('#password').addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;
            if (key === 13) { // 13 is enter
                FAMSLogin();
            }
        });
        function FAMSLogin() {
            window.location.assign("Main.aspx");
            sessionStorage.setItem("Account_ID", 1);
            sessionStorage.setItem("Account_Count", 1);
            sessionStorage.setItem("Username", 'PicknPay - NorthGate');
            sessionStorage.setItem("Password",'PicknPay');
            sessionStorage.setItem("Login", 1);
            sessionStorage.setItem("Role_ID", 6);


            //callBlock();
            //clearListCookies();
            //localStorage.clear();
            //sessionStorage.clear();
            //document.cookie = 'ReportServerAuth=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            //document.cookie = 'DashboardServerAuth=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';


            //var user = document.getElementById('username').value;
            //var pass = document.getElementById('password').value;
            //callBlock();
            //$.ajax({
            //    url: weburl + 'queryType=SqlSp&sp=Login&paramlist=' + user + ',' + pass + "&trick=" + (new Date()).getMilliseconds(),
            //    context: document.body,
            //    async: false,
            //    success: function (rec) {
            //        var json = JSON.parse(rec);
            //        if (json.length > 0) {
            //            if (json[0].Account > 1) {
            //                $("#loginform").addClass("display-none");
            //                $("#accountselector").removeClass("display-none");
            //                sessionStorage.setItem("User_ID", json[0].ID);
            //                getAccounts();
            //            }
            //            else if (json[0].Account = 1) {
            //                sessionStorage.setItem("Account_Name", json[0].Account_Name);
            //                sessionStorage.setItem("User_ID", json[0].ID);
            //                window.location.assign("Main.aspx");
            //            }

            //            sessionStorage.setItem("Account_ID", json[0].Account_ID);
            //            sessionStorage.setItem("Account_Count", json[0].Account);
            //            sessionStorage.setItem("Username", json[0].Username);
            //            sessionStorage.setItem("Password", pass);
            //            sessionStorage.setItem("Login", 1);
            //            sessionStorage.setItem("Role_ID", json[0].Role_ID);
            //        }

            //        else {
            //            alert('incorrect login details');
            //        }
            //    },
            //    error: function (e) {

            //     //   alert(e.error);
            //     //   alert(e);
            //    }
            //});
        }

        function getAccounts() {
            //$("#accountlist").append($("<li>").text("Some Text."));
            //$("#accountlist").append("<li id='11'>list item</li>");
            //$("#accountlist ul").append('<li>Message Center</li>');
            //$("#accountlist ul li").last().html('<li> Menu 5 </li>');

            $.ajax({
                dataType: "json",
                contentType: "application/json;charset=UTF-8",
                type: "GET",
                url: weburl + 'queryType=SqlSp&sp=GetAllAccounts_byUserID&paramList=' + sessionStorage.getItem("User_ID") + "&trick=" + (new Date()).getMilliseconds(),
                async: false,
                traditional: true,
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("application/j-son;charset=UTF-8");
                    }
                },

                success: function (data) {
                    var _len = data.length
                        , post, i;
                    for (i = 0; i < _len; i++) {
                        post = data[i];
                        load('1', 'hvac')

                        //$("#accountlist").append('<li class="mt-list-item" onClick=load(' + post.Account_ID + ',"' + encodeURI(post.Account_Name) + '") style="padding:5px 10px 5px 10px;"><div style="padding:5px 10px 5px 0px;" class="list-item-content" ><h3  class="uppercase"><a style="font-size:18px" onClick=load(' + post.Account_ID + ',"' + encodeURI(post.Account_Name) + '")>' + post.Account_Name + '</a></h3></div></li>');
                        //$("#accountlist").append($("<li class='mt-list-item'><div class='list-item-content'><h3 class='uppercase'>sweeet</h3></div></li>'");
                        //var item = document.getElementById('accountlist');
                        //item.innerHTML += '<li id=' + i + '>Fifth</li>';

                    }

                },
                error: function (e) {

                    alert(e.error);
                    alert(e);
                }

            });

        }
        $(document).ready(function () {

            $("ul[id*=accountlist] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
            });

            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
            });

        });


        function load(a, b) {
            sessionStorage.setItem("Account_ID", a);
            sessionStorage.setItem("Account_Name", unescape(b))
            window.location.replace("Main.aspx");

        }
        function gotomainjs() {
            $.ajax({
                type: "POST",
                url: 'login.aspx/gotomain',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json"

            });
        }


        $.backstretch([
            "./res/img/1.jpg",
            "./res/img/2.jpg",
            "./res/img/3.jpg"
        ], {
                fade: 1000,
                duration: 8000
            }
        );
    </script>
</body>
</html>
