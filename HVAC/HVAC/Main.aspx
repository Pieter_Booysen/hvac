﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="HVAC.Main" %>

<!DOCTYPE html>


<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>HVAC PORTAL LANDING PAGE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="FAMS PORTAL LANDING PAGE" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="res/css/hvac.css" rel="stylesheet" />

  <%--  <link href="assets/layouts/layout2/css/themes/grey.min.css" rel="stylesheet" type="text/css" id="style_color" />--%>
    <link href="assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<body data-style="dark" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-fixed page-sidebar-mobile-offcanvas">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
              
                    <img src="res/img/hvac.png"  alt="logo" class="logo-default" style=" display:block;margin:auto; max-width: 100%; max-height: 95%;"/>
               
                <div class="menu-toggler sidebar-toggler">
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a id="navtogbutton"  href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE ACTIONS -->
            <!-- DOC: Remove "hide" class to enable the page header actions -->
            <!-- END PAGE ACTIONS -->
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">

                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li id="famstoolscontainer" class="dropdown dropdown-user hide">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <!--<img alt="" class="img-circle" src="./res/img/Logo.png" />-->
                                <span class="username username-hide-on-mobile">Tools</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul id="famstools" class="dropdown-menu dropdown-menu-default">
                            </ul>

                        </li>

                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <!--<img alt="" class="img-circle" src="./res/img/Logo.png" />-->
                                <span id="lblAccount" class="username username-hide-on-mobile"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul id="accountlist" style="height: auto; max-height: 400px; overflow-x: hidden;" class="dropdown-menu" role="menu">
                            </ul>

                        </li>

                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <!--<img alt="" class="img-circle" src="./res/img/Logo.png" />-->
                                <span id="lblUsername" class="username username-hide-on-mobile"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a onclick="logout()">
                                        <i class="icon-key"></i>Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed navxxx-->
                <ul class="page-sidebar-menu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                    
                    <li id="navQuick" class="nav-item hide  ">
                        <a class="nav-link nav-toggle">
                            <i class="icon-pie-chart"></i>
                            <span class="title">Quick Report/View</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li id="navQVIO" onclick="" class="nav-item  ">
                                <a onclick="pageControler('loadQVIO', 1, 'Dashboard', 'pages/Dashboard.aspx', 'navQVIO')" class="nav-link ">
                                    <i class="icon-drop"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                         <%--   <li id="navQVGrid" onclick="" class="nav-item  ">
                                <a onclick="pageControler('loadQVGrid', 2, 'Quick View - Grid', 'pages/QuickReport.aspx', 'navQVGrid')" class="nav-link ">
                                    <i class="fa fa-truck"></i>
                                    <span class="title">Quick Grid View</span>
                                </a>
                            </li>--%>
                             <li id="navQVAnl" onclick="" class="nav-item  ">
                                <a onclick="pageControler('loadQVGrid', 3, 'Analystics', './PowerBi.aspx', 'navQVGrid')" class="nav-link ">
                                    <i class="fa fa-truck"></i>
                                    <span class="title">Analytics</span>
                                </a>
                            </li>
                         <li id="navQVQuick" onclick="" class="nav-item  ">
                                <a onclick="pageControler('loadQVQuick', 4, 'HVAC Quick Reports', './Reporting/FAMSQuickReport.aspx', 'navQVQuick')" class="nav-link ">
                                    <i class="fa fa-truck"></i>
                                    <span class="title">HVAC Quick View</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li id="navReport" class="nav-item hide  ">
                        <a class="nav-link nav-toggle">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Custom/Scheduled Reports</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li id="navReportsStandard" onclick="loadReport()" class="nav-item  ">
                                <a onclick="" class="nav-link ">
                                    <i class="fa fa-tasks"></i>
                                    <span class="title">HVAC Reports</span>
                                </a>
                            </li>                     
                        </ul>
                    </li>

                    <li id="navManager" class="nav-item  hide">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-wrench"></i>
                            <span class="title">HVAC Manager</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-cogs"></i>
                                    <span class="title">Station</span>
                                    <span class="arrow"></span>
                                </a>
                                <%--xxxxxxxxxx--%>
                                <ul class="sub-menu">
                                    <li id="navManagerStation" class="nav-item ">
                                        <a onclick="pageControler('loadManagerStation', 3, 'Manager - Station', 'pages/Station.aspx', 'navManagerStation')"
                                            class="nav-link ">Configure Station
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-cogs"></i>
                                    <span class="title">Sensor</span>
                                    <span class="arrow"></span>
                                </a>
                                <%--xxxxxxxxxx--%>
                                <ul class="sub-menu">
                                    <li id="navManagerSensor" class="nav-item ">
                                        <a onclick="pageControler('loadManagerSensor', 5, 'Manager - Sensor', 'pages/Sensor.aspx', 'navManagerSensor')"
                                            class="nav-link ">Configure Sensor
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div id="content" class="page-content" style="overflow: auto; padding: 0 0 0 0;">
                <div id="tab" class="easyui-tabs hide" fit="true" style="">
                </div>
            </div>


            <%--  <div id="dd" name="LoadInformation"></div>--%>

            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        <!-- BEGIN QUICK SIDEBAR -->
        <!-- END QUICK SIDEBAR -->
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <!--<div class="page-footer">
        <div class="page-footer-inner">
            2016 &copy; Metronic Theme By
            <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
            <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>-->
    <!-- END FOOTER -->
    <!-- BEGIN QUICK NAV -->

    <div class="quick-nav-overlay"></div>
    <!-- END QUICK NAV -->
    <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script>
        <script src="../assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
    <script src="assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <link rel="stylesheet" type="text/css" href="assets/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="assets/easyui/icon.css">
    <script type="text/javascript" src="assets/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="assets/easyui/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="assets/easyui/datagrid-filter.js"></script>
    <script src="assets/DetectMobile/detectmobilebrowser.js"></script>

    <script>
        //moblie check


        var accounthasChanged = false;
        var isMobile = jQuery.browser.mobile;
        // isMobile = true;
        window.onload = function () {

            if (sessionStorage.getItem("Login") != 1) {
                window.location = "Login.aspx";
            }
            if (isMobile == false) {
                $("#tab").removeClass("hide");
            }
            if (checkRole() == true) {
                loadtools();
                pageControler('dash', 1, 'Dashboard', 'pages/dashboard.aspx', 'navQVATG')
                
                
            }
          //  getAccounts();


            $("#lblUsername").text(sessionStorage.getItem("Username"));
            $("#lblAccount").text(sessionStorage.getItem("Account_Name"));


        }

        function pageControler(name, tabId, title, url, navItem) {
            // callBlock();
            //f_addTab('PowerBi', 25, 'PowerBi', './Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") + '&dash=0');
            //clearSelected();

            if (url == 'bidash0') {
                url = './Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") +'&userID=' + sessionStorage.getItem("User_ID")+ '&dash=0';
            }
            if (url == 'bidash1') {
                url = './Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") +'&userID=' + sessionStorage.getItem("User_ID")+ '&dash=1';
            }
            //alert(url);
            if (isMobile == false) {
                callBlock();
                f_addTab(name, tabId, title, url);
                clearSelected();
            }
            else {
                callBlock();
                document.getElementById("content").innerHTML = '<object style="width:98%;height:90vh" type="text/html" data="' + url + '" ></object>';
                clearSelected();
                // $("#" + navItem).addClass("active open");
                if (title !== 'Start Dashboard') {
                     document.getElementById("navtogbutton").click();
                }
               
            }
        }

        /////xxxxxx
        function refreshTab(a, b) {

            sessionStorage.setItem("Account_ID", a);
            sessionStorage.setItem("Account_Name", unescape(b))

            if (isMobile == false) {

                var pp = $('#tab').tabs('getSelected');


                if (pp[0].innerHTML.indexOf('Powerbi.aspx') >= 0) {

                    if (pp[0].innerHTML.indexOf('dash=1') >= 0) {
                        pp[0].innerHTML = '<iframe name=\"Analytics\" src=\"./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID")+'&amp;userID=' + sessionStorage.getItem("User_ID") + '&amp;dash=1\" frameborder=\"0\" style=\"width: 100%; height: 100%;\" \"=\"\"></iframe>';
                    }
                    if (pp[0].innerHTML.indexOf('dash=0') >= 0) {
                        pp[0].innerHTML = '<iframe name=\"Analytics\" src=\"./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID")+'&amp;userID=' + sessionStorage.getItem("User_ID") + '&amp;dash=0\" frameborder=\"0\" style=\"width: 100%; height: 100%;\" \"=\"\"></iframe>';
                    }
                }
                else {
                    pp[0].innerHTML = pp[0].innerHTML;
                }
                pp.panel('refresh');
            }
            else {
                sessionStorage.setItem("Account_ID", a);
                sessionStorage.setItem("Account_Name", unescape(b))
                // window.location.replace("Main.aspx");
                var variable = document.getElementById("content").innerHTML;

                if (variable.indexOf('Powerbi.aspx') >= 0) {

                    if (variable.indexOf('dash=1') >= 0) {
                        variable = '<object style="width:98%;height:90vh" type="text/html" data="./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") +'&userID=' + sessionStorage.getItem("User_ID") +  '&dash=1" ></object>';
                    }
                    if (variable.indexOf('dash=0') >= 0) {
                        variable = '<object style="width:98%;height:90vh" type="text/html" data="./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") +'&userID=' + sessionStorage.getItem("User_ID") + '&dash=0" ></object>';
                    }
                }
                document.getElementById("content").innerHTML = variable;
            }

            $("#lblAccount").text(sessionStorage.getItem("Account_Name"));
            accounthasChanged = true
        }
        function f_addTab(name, tabId, title, url) {

            if ($("#tab").tabs('exists', title)) {
                $('#tab').tabs('select', title);
                var pp = $('#tab').tabs('getSelected');

                pp[0].innerHTML = pp[0].innerHTML;
                pp.panel('refresh');
                // $('#'+tabId).attr('src',url);//Refresh
            } else {
                $('#tab').tabs('add', {
                    id: tabId,
                    title: title,
                    content: '<iframe name="' + name + '" src="' + url + '" frameborder="0" style="height:98%;width:98%;" "></iframe>',
                    closable: true,//Tab shows the close button
                    cache: true //Set the cache, if false, selected in each selected tab, will load a page content
                });
            }

        }
        $('#tab').tabs({
            onSelect: function () {
                if (accounthasChanged == true) {
                    var pp = $('#tab').tabs('getSelected');

                    if (pp[0].innerHTML.indexOf('Powerbi.aspx') >= 0) {

                        if (pp[0].innerHTML.indexOf('dash=1') >= 0) {
                            pp[0].innerHTML = '<iframe name=\"Analytics\" src=\"./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") +'&amp;userID=' + sessionStorage.getItem("User_ID") + '&amp;dash=1\" frameborder=\"0\" style=\"width: 100%; height: 100%;\" \"=\"\"></iframe>';
                        }
                        if (pp[0].innerHTML.indexOf('dash=0') >= 0) {
                            pp[0].innerHTML = '<iframe name=\"Analytics\" src=\"./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID")  +'&amp;userID=' + sessionStorage.getItem("User_ID") + '&amp;dash=0\" frameborder=\"0\" style=\"width: 100%; height: 100%;\" \"=\"\"></iframe>';
                        }
                    }
                    else {
                        pp[0].innerHTML = pp[0].innerHTML;
                    }
                    pp.panel('refresh');
                }

            }

        })
        var weburl = "<%=ConfigurationManager.AppSettings["webApiURL"]%>";
        function callBlock() {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            setTimeout($.unblockUI, 2500);
        }
        function checkRole() {
            function onlyATG() {
                $("#navQVGrid").addClass("hide");
            }
            function onlyFams() {
                $("#navQVATG").addClass("hide");
            }

            var roleid = sessionStorage.getItem("Role_ID");
            if (roleid == 6) {
                $("#navAdmin").removeClass("hide");
                $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                $("#navReport").removeClass("hide");
                $("#navManager").removeClass("hide");
                $("#navDataIntegrity").removeClass("hide");
                $("#navFAMSManagerOptional").removeClass("hide");
                $("#navPortalUserSet").removeClass("hide");
                $("#famstoolscontainer").removeClass("hide");

            }
            else if (roleid == 1) {
                $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                $("#navReport").removeClass("hide");
                //   onlyFams();
            }
            else if (roleid == 2) {
                $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                $("#navReport").removeClass("hide");
                $("#navManager").removeClass("hide");
                $("#navDataIntegrity").removeClass("hide");
                $("#navFAMSManagerOptional").removeClass("hide");
                $("#navPortalUserSet").removeClass("hide");
                $("#famstoolscontainer").removeClass("hide");

            }
            else if (roleid == 3) {
               // $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                
                //   onlyATG();
            }
            else if (roleid == 4) {
                $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                $("#navReport").removeClass("hide");
                $("#navManager").removeClass("hide");
                $("#navDataIntegrity").removeClass("hide");
                $("#navFAMSManagerOptional").removeClass("hide");
                $("#navPortalUserSet").removeClass("hide");
                //  $("#famstoolscontainer").removeClass("hide");
                //   onlyATG();
            }
            else if (roleid == 5) {
                $("#navDash").removeClass("hide");
                $("#navQuick").removeClass("hide");
                $("#navReport").removeClass("hide");
                $("#navManager").removeClass("hide");
                $("#navDataIntegrity").removeClass("hide");
                $("#navFAMSManagerOptional").removeClass("hide");
                $("#navPortalUserSet").removeClass("hide");
                //   $("#famstoolscontainer").removeClass("hide");
            }
            else {
                $.messager.alert('Alert', 'No role assigned to user', function (r) { });
                return false;
            }

            return true;
        }
        function loadReport() {
            callBlock();
            window.open('http://portal.fams.co.za/syncfusionreportlogin/home.aspx?u=' + 'hvac' + '&p=' + 'Hvac123!' + '&t=report', '_blank');
        }
        function loadReportDashboard() {
            callBlock();
            window.open('http://portal.fams.co.za/syncfusiondashlogin/home.aspx?u=' + sessionStorage.getItem("Username") + '&p=' + sessionStorage.getItem("Password") + '&t=dash', '_blank');
        }
        function loadReportPowerbi() {
            callBlock();
            f_addTab('ReportPowerbi', 8, 'ReportPowerbi', './Reporting/ReportsPowerBi.aspx');
            clearSelected();

            //callBlock();
            //document.getElementById("content").innerHTML = '<object style="width:100%;height:80vh" type="text/html" data="./Reporting/ReportsPowerBi.aspx" ></object>';
            //clearSelected();
            //$("#navReportsPowerbi").addClass("active open");
        }

        function logout() {
            sessionStorage.clear();
            window.location = "Login.aspx";
        }
        function clearSelected() {

            $("#navManagerStation").removeClass("active open");      
            $("#ManualEntry").removeClass("active open");
            $("#navQuick").removeClass("active open");
            $("#BatchImport").removeClass("active open");
            $("#DataImports").removeClass("active open");
            $("#DataIntegrity").removeClass("active open");
            $("#navOperatorStore").removeClass("active open");
            $("#navManagerAccountProduct").removeClass("active open");
            $("#navManagerStore").removeClass("active open");
            $("#navDash").removeClass("active open");
            $("#navGrid").removeClass("active open");
            $("#navChart").removeClass("active open");
            $("#navTest").removeClass("active open");
            $("#navReport").removeClass("active open");
            $("#navReportDashboard").removeClass("active open");
            $("#navReportsPowerbi").removeClass("active open");
            $("#navManagerStore").removeClass("active open");
            $("#navManagerTank").removeClass("active open");
            $("#navManagerMasterEquipment").removeClass("active open");
            $("#navManagerCostCentre").removeClass("active open");
            $("#navManagerAllocation").removeClass("active open");
            $("#navManagerSite").removeClass("active open");
            $("#navUsertoSite").removeClass("active open");
            $("#navManagerEqp2").removeClass("active open");
            $("#navManagerEqpParam2").removeClass("active open");
            $("#navOperator").removeClass("active open");
            $("#navUserAccount").removeClass("active open");
            $("#navManagerEquipmentMaster").removeClass("active open");
            $("#navManagerEquipmentCostCentre").removeClass("active open");
            $("#navFilterDispensing").removeClass("active open");
            $("#navFilterReceiving").removeClass("active open");
            $("#navFilterTransfer").removeClass("active open");
            $("#navManagerAnalytics").removeClass("active open");
            $("#navManagerAnalytics2User").removeClass("active open");
            $("#navDash").removeClass("active open");
            $("#navManagerPerAllocationStore").removeClass("active open");
            $("#navStoreSite").removeClass("active open");
            $("#navAdmin").removeClass("active open");
            $("#navNewAccount").removeClass("active open");


        }
        function getAccounts() {
            $.ajax({
                dataType: "json",
                contentType: "application/json;charset=UTF-8",
                type: "GET",
                url: weburl + 'queryType=SqlSp&sp=GetAllAccounts_byUserID&paramList=' + sessionStorage.getItem("User_ID") + "&trick=" + (new Date()).getMilliseconds(),
                async: false,
                traditional: true,
                beforeSend: function (x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("application/j-son;charset=UTF-8");
                    }
                },

                success: function (data) {

                    var _len = data.length
                        , post, i;

                    for (i = 0; i < _len; i++) {
                        post = data[i];
                        $("#accountlist").append('<li><a onClick=refreshTab(' + post.Account_ID + ',"' + encodeURI(post.Account_Name) + '")> ' + post.Account_Name + '</a></li>');
                        //$("#accountlist").append($("<li class='mt-list-item'><div class='list-item-content'><h3 class='uppercase'>sweeet</h3></div></li>'");
                        //var item = document.getElementById('accountlist');
                        //item.innerHTML += '<li id=' + i + '>Fifth</li>';

                    }

                },
                error: function (e) {
                    //  alert(e.error);
                    //  alert(e);
                    window.location = "Login.aspx";
                }

            });

        }

        function loadtools() {
            // $("#famstools").append('<li><a onClick=load(' + post.Account_ID + ',"' + encodeURI(post.Account_Name) + '")> ' + post.Account_Name + '</a></li>');
            //onclick="pageControler('loadManagerManualEntry', 15, 'Integrity - Manual Entry', './Manager/ManualEntries.aspx', 'ManualEntry')" class="nav-item ">
            $("#famstools").append('<li><a onClick="">Future Events</a></li>');
        }
        function loadWindow() {
            pageControler('loadManagerManualEntry', 15, 'Integrity - Manual Entry', './Manager/ManualEntries.aspx', 'ManualEntry');
            //$('#dd').dialog({
            //    title: 'Manual Entries',
            //    width: 640,
            //    height: 550,
            //    closed: false,
            //    cache: false,
            //    modal: false,
            //    padding: 20,
            //    collapsible: true
            //})
            //$('#dd').dialog({

            //}).load('/manager/manualentries.aspx');
            // window.open("manager/manualentries.aspx","", "width=500, height=480,directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no");
            //  window.focus;
        }
        function load(a, b) {
            sessionStorage.setItem("Account_ID", a);
            sessionStorage.setItem("Account_Name", unescape(b))
            // window.location.replace("Main.aspx");
            //var variable = document.getElementById("content").innerHTML;
            $("#lblAccount").text(sessionStorage.getItem("Account_Name"));

            //if (variable.indexOf('Powerbi.aspx') >= 0) {
            //    if (variable.indexOf('dash=1') >= 0) {

            //        variable = '<object style="width:100%;height:85vh " type="text/html" data="./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") + '&dash=1" ></object>';
            //    }
            //    if (variable.indexOf('dash=0') >= 0) {

            //        variable = '<object style="width:100%;height:85vh " type="text/html" data="./Powerbi.aspx?param=' + sessionStorage.getItem("Account_ID") + '&dash=0" ></object>';
            //    }

            //}

            //document.getElementById("content").innerHTML = variable;

        }

        //////////////////////////////////////

    </script>
</body>
</html>
