﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="HVAC.pages.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
<link href="../assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="../assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="../assets/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="../assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<body>
  <div class="page-container">
	<!-- BEGIN SIDEBAR -->
	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->
			
			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				
				<div class="col-md-12">
                    <!-- xxx 2015/08/03-->
					<div id="dashboard-report-range" class="pull-right tooltips btn btn-sm btn-default" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
						<i class="page-toolbar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block" id="mdate"></span>&nbsp; <i class="fa fa-angle-down"></i>
					</div>
				</div>
			</div>
			<h1 class="page-title">Pick n Pay NorthGate</h1> 
			HVAC Dashboard <small>real time data</small>

			<!-- END PAGE HEADER-->
			<!-- BEGIN DASHBOARD STATS xxxxxxxxxchanges-->
        
			<div class="row">             
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                     <div class="number"><h1>Temperatures</h1></div>
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Indoor Temp 1 : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>               
					</div>
                    <div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Indoor Temp 2 : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:12
							</div>
						</div>
					</div>
                    <div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Indoor Temp 3 : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>               
					</div>
                    <div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Outdoor Temp : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>               
					</div>
                    <div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Supplied Air : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>               
					</div>
                    <div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Returned Air : 23.2C
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>               
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="number"><h1>CO2 Levels</h1></div>
					<div class="dashboard-stat red-intense">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Sensor 1 : 15 PPM
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>
                    <div class="dashboard-stat red-intense">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Sensor 2 : 12 PPM
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>

                      <div class="number"><h1>Energy Information</h1></div>
					<div class="dashboard-stat green-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Energy : 25Kwhr
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>
                    <div class="dashboard-stat green-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Energy (Total) : 1225Kwhr      
							</div>
							<div class="desc">
								Date : 2018/01/01 - Current
							</div>
						</div>     
					</div>

                  
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                    <div class="number"><h1>Digital Status</h1></div>
                    <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Cooling Plant 1- On     
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
                                Status : Heating
							</div>
						</div>     
					</div>

                     <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Cooling Plant 2 - On        
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
                                 Status : Cooling
							</div>
						</div>     
					</div>

                     <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Supply Air 1 : 2.5 m/S       
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>

                      <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">Supply Air 2 : 2.5 m/S     
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>

				</div>   
                
                <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">

                    <div class="number"><h1>Cooling Plant</h1></div>
                    <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">CP1 - Cooling     
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>

                     <div class="dashboard-stat yellow-haze">
					<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">CP2 - Heating            
							</div>
							<div class="desc">
								Date : 2018/11/28 12:36
							</div>
						</div>     
					</div>

				</div>  

     </div>
				





			</div>       
	</div>
  <%--  <div class="row">
				<div class="col-md-4">
					<!-- BEGIN Portlet PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Call Elevated step to Emergency Call
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" data-load="true" data-url="" class="reload" onclick="getNC_CallElevateEmergencyCancel()">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
							</div>
                            <div class="actions">
                                <a class="btn btn-default btn-sm"  onclick="exportClicked()">Export</a>
                            </div>
						</div>
						<div class="portlet-body portlet-empty">

                          
						</div>
					</div>
					<!-- END Portlet PORTLET-->
				</div>         
			</div>--%>
	
      <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../assets/global/plugins/flot/jquery.flot.min.js"></script>
<script src="../assets/global/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../assets/global/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../assets/global/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="../assets/global/plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="../assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-daterangepicker/daterangepickerCustom.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="../assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="../assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="../assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="../assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="../assets/admin/pages/scripts/charts-flotcharts.js"></script>
<script src="../assets/global/scripts/cusChart.js" type="text/javascript"></script>
<script src="../assets/global/scripts/cusGeneral.js" type="text/javascript"></script>
	<%--<!-- END QUICK SIDEBAR -->
    <div id="Div0">
        <iframe id="myMapIframeData" class="framestyle" style="height: 550px; width: 100%;">
        </iframe>
    </div>--%>
                   
</div>
</body>
</html>
