﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sensor.aspx.cs" Inherits="HVAC.pages.Sensor" %>

<!DOCTYPE html>
<html>
<head>
    <title>Sensor</title>
	<meta charset="utf-8" />
</head>
<body>

    <link rel="stylesheet" type="text/css" href="../assets/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="../assets/easyui/icon.css">
    <script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="../assets/easyui/datagrid-filter.js"></script>
    <script src="../assets/FAMS/FamsFunctions.js"></script>
    <script src="../assets/Promise/promise-7.0.4.min.js"></script>
    <!-- END PAGE HEADER-->
  
    <table id="dg" title="HVAC Sensor Configuration" style="height:620px" toolbar="#toolbar">


    </table>
    <div id="toolbar">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newItem()">New Sensor</a>
         <a href="#" class="easyui-linkbutton" iconcls="icon-reload" plain="true" onclick="$('#dg').datagrid('reload');">Reload</a>
    </div>
    <div id="dlg" class="easyui-dialog" style="width:500px"
         closed="true" buttons="#dlg-buttons" >
        <form id="fm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:20px;font-size:14px;border-bottom:1px solid #ccc">New Sensor</div>
            <div style="margin-bottom:10px">
                <input id="Name" name="Name" class="easyui-textbox" required="true" label="Name:" labelWidth="200px" style="width:100%">
                <input id="EngUnit" name="EngUnit" class="easyui-textbox" required="true" label="EngUnit:" labelWidth="200px" style="width:100%">
                <input id="UseFor" name="UseFor" class="easyui-textbox" required="true" label="UseFor:" labelWidth="200px" style="width:100%">
                <input id="Nr" name="NR" class="easyui-textbox" required="true" label="Nr:" labelWidth="200px" style="width:100%">
                <input id="SensorType" name="SensorType" class="easyui-combobox" label="SensorType:" labelWidth="200px" style="width:100%" >
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconcls="icon-ok" onclick="saveItem()" style="width: 90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width: 90px">Cancel</a>
    </div>


    <script>

        var weburl = "<%=ConfigurationManager.AppSettings["webApiURL"]%>";
        var weburl2 = "<%=ConfigurationManager.AppSettings["webApiURL2"]%>";
        var fams = Object.create(famsfunc);
        fams.init(weburl, weburl2);

        $('#SensorType').combobox({
            url: weburl + 'queryType=SqlQuery&sp=select *from sensor',
            valueField: 'ID',
            textField: 'Name'
        });

        function newItem() {
            $('#dlg').dialog('open').dialog('center').dialog('setTitle', 'New Store');
            $('#fm').form('clear');
          
        }
        function saveItem() {
             
        }
        $('#dg').datagrid({
            url: weburl + 'queryType=SqlSp&sp=CRUD_Sensor_Get&paramlist=' + 12,
            contentType: "application/json",
            singleSelect: true,
            columns: [[
                 
                  { field: 'SensorName', title: 'SensorName', width: 80, editor: 'text' },
                  { field: 'Usagefor', title: 'Usagefor', width: 200, editor: 'text' },
                  { field: 'EngUnit', title: 'EngUnit', width: 100, editor: 'text' },
                  { field: 'Nr', title: 'Nr', width: 60, editor: 'text' },
                  {
                      field: 'action', title: 'Action', width: 60, align: 'center',
                      formatter: function (value, row, index) {
                          if (row.editing) {
                              var s = '<a href="javascript:void(0)" onclick="saverow(this)">Save</a> ';
                              var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancel</a>';
                              return s + c;
                          } else {
                              var e = '<a href="javascript:void(0)" onclick="editrow(this)">Edit</a> ';
                              var d = '<a href="javascript:void(0)" onclick="deleterow(this)">Delete</a>';
                              return e + d;
                          }
                      }
                  }
            ]],
            onEndEdit: function (index, row) {
                $.ajax
                  ({
                    type: "GET",
                    url: weburl2 + 'queryType=SqlSp&sp=CRUD_Storess_Edit&paramlist=' + row.ID + ',' + row.StoreTypeID + ',' + row.TimeZoneID + ',' + row.HardwareID + ',' + row.Name + ',' + row.Macaddress + ',' + row.IPAddress + ',' + row.CellNumber + ',' + row.Enable + ',' + row.Lat + ',' + row.Long,
                        success: function (html) {
                           $.messager.alert('Success', 'Successfully updated', function (r) { });
                     }
                 });
                var ed = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'StoreTypeID'
                });
                var ed2 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'TimeZoneID'
                });
                var ed3 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'HardwareID'
                });
                var ed4 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'Enable'
                });
                row.StoreType = $(ed.target).combobox('getText');
                row.Timezone = $(ed2.target).combobox('getText');
                row.Hardware = $(ed3.target).combobox('getText');
                row.Enable = $(ed4.target).combobox('getText');
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                $(this).datagrid('refreshRow', index);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            }

        }).datagrid('enableFilter');
        function getRowIndex(target) {
            var tr = $(target).closest('tr.datagrid-row');
            return parseInt(tr.attr('datagrid-row-index'));
        }
        function editrow(target) {
            $('#dg').datagrid('beginEdit', getRowIndex(target));
        }
        function deleterow(target) {
             fams.deletefromtable(getRowIndex(target), $('#dg').datagrid('getData'), 'CRUD_Store_Remove').then(function () {
                $('#dg').datagrid('reload'); 
            });
            //$.messager.confirm('Confirm', 'Are you sure?', function (r) {
            //    if (r) {
            //        var row = $('#dg').treegrid('getSelected');
            //        $.ajax
            //         ({
            //         type: "GET",
            //         url: weburl + 'queryType=SqlSp&sp=CRUD_Store_Remove&paramlist=' + row.ID,
            //         success: function (html) {
            //             $('#dg').datagrid('deleteRow', getRowIndex(target));
            //             var inserted = $('#dg').datagrid('getChanges', 'deleted');
            //             $.messager.alert('Success', 'Successfully removed', function (r) { });
            //         }
            //     });
            //    }
            //});
        }
        function saverow(target) {
            $('#dg').datagrid('endEdit', getRowIndex(target));
          

        }
        function cancelrow(target) {
            $('#dg').datagrid('cancelEdit', getRowIndex(target));
        }
        function insert() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                var index = $('#dg').datagrid('getRowIndex', row);
            } else {
                index = 0;
            }
            $('#dg').datagrid('insertRow', {
                index: index,
                row: {
                    status: 'P'
                }
            });
            $('#dg').datagrid('selectRow', index);
            $('#dg').datagrid('beginEdit', index);
        }    
    </script>
</body>
</html>

