﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Station.aspx.cs" Inherits="HVAC.Station" %>

<!DOCTYPE html>
<html>
<head>
    <title></title>
	<meta charset="utf-8" />
</head>
<body>

    <link rel="stylesheet" type="text/css" href="../assets/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="../assets/easyui/icon.css">
    <script type="text/javascript" src="../assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../assets/easyui/jquery.edatagrid.js"></script>
    <script type="text/javascript" src="../assets/easyui/datagrid-filter.js"></script>
    <script src="../assets/FAMS/FamsFunctions.js"></script>
    <script src="../assets/Promise/promise-7.0.4.min.js"></script>
    <!-- END PAGE HEADER-->
  
    <table id="dg" title="HVAC Store Configuration" style="height:620px" toolbar="#toolbar">


    </table>
    <div id="toolbar">
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newItem()">New Store</a>
         <a href="#" class="easyui-linkbutton" iconcls="icon-reload" plain="true" onclick="$('#dg').datagrid('reload');">Reload</a>
    </div>
    <div id="dlg" class="easyui-dialog" style="width:500px"
         closed="true" buttons="#dlg-buttons" >
        <form id="fm" method="post" novalidate style="margin:0;padding:20px 50px">
            <div style="margin-bottom:20px;font-size:14px;border-bottom:1px solid #ccc">New Store</div>
            <div style="margin-bottom:10px">
                <input id="Name" name="Name" class="easyui-textbox" required="true" label="Name:" labelWidth="200px" style="width:100%">
                <input id="Macaddress" name="Macaddress" class="easyui-textbox" required="true" label="Macaddress:" labelWidth="200px" style="width:100%">
                <input id="IP" name="IPAddress" class="easyui-textbox" required="true" label="IP:" labelWidth="200px" style="width:100%">
                <input id="CellNumber" name="CellNumber" class="easyui-textbox" required="true" label="CellNumber:" labelWidth="200px" style="width:100%">
                <input id="Lat" name="Lat" class="easyui-textbox" label="Lat:" labelWidth="200px" style="width:100%">
                <input id="Long" name="Long" class="easyui-textbox" label="Long:" labelWidth="200px" style="width:100%">
                <input id="StoreType" name="StoreType" class="easyui-combobox" label="Store Type:" labelWidth="200px" style="width:100%" >
                <input id="Timezone" name="Timezone" class="easyui-combobox" label="Timezone:" labelWidth="200px" style="width:100%" >
                <input id="Hardware" name="Hardware" class="easyui-combobox" label="Hardware Type:" labelWidth="200px" style="width:100%">
                <select id="Enable" class="easyui-combobox" label="Enable:" name="Enable" labelWidth="200px" style="width:100%">
                    <option value="true">true</option>
                    <option value="false">false</option>
                </select>
            </div>
        </form>
    </div>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton c6" iconcls="icon-ok" onclick="saveItem()" style="width: 90px">Save</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconcls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width: 90px">Cancel</a>
    </div>


    <script>

        var weburl = "<%=ConfigurationManager.AppSettings["webApiURL"]%>";
        var weburl2 = "<%=ConfigurationManager.AppSettings["webApiURL2"]%>";
        var fams = Object.create(famsfunc);
        fams.init(weburl, weburl2);

        $('#Timezone').combobox({
            url: weburl + 'queryType=SqlSp&sp=Get_Timezone&paramlist=0',
            valueField: 'ID',
            textField: 'Name'
        });
        $('#StoreType').combobox({
            url: weburl + 'queryType=SqlSp&sp=Get_Storetype&paramlist=0',
            valueField: 'ID',
            textField: 'Name'
        });
        $('#Hardware').combobox({
            url: weburl + 'queryType=SqlSp&sp=Get_Hardware&paramlist=0',
            valueField: 'ID',
            textField: 'Name'
        });


        function newItem() {
            $('#dlg').dialog('open').dialog('center').dialog('setTitle', 'New Store');
            $('#fm').form('clear');
          
        }
        function saveItem() {
              
        }
        $('#dg').datagrid({
            url: weburl + 'queryType=SqlSp&sp=CRUD_Station_Get&paramlist=' + 12,
            contentType: "application/json",
            singleSelect: true,
            columns: [[
                 
                  { field: 'Name', title: 'Station', width: 80, editor: 'text' },
                  { field: 'Macaddress', title: 'Mac address', width: 100, editor: 'text' },
                  { field: 'IPAddress', title: 'IP Address', width: 70, editor: 'text' },
                  { field: 'CellNumber', title: 'Cell Number', width: 60, editor: 'text' },
                  { field: 'Enable', title: 'Enabled', width: 50,
                      formatter:function(value,row){
                          return row.Enable || value;
                      },
                      editor: {
                              type: 'combobox',
                              options: {
                              valueField: 'ID',
                              textField: 'Name',
                              data: [{
                                  ID: 'true',
                                  Name: 'true'
                              },{
                                  ID: 'false',
                                  Name: 'false'
                              }]
                             ,
                              required: true
                              }
                      }
                  },
                   { field: 'Lat', title: 'Lat', width: 50, editor: 'text' },
                  { field: 'Long', title: 'Long', width: 50, editor: 'text' },
                  {
                      field: 'action', title: 'Action', width: 60, align: 'center',
                      formatter: function (value, row, index) {
                          if (row.editing) {
                              var s = '<a href="javascript:void(0)" onclick="saverow(this)">Save</a> ';
                              var c = '<a href="javascript:void(0)" onclick="cancelrow(this)">Cancel</a>';
                              return s + c;
                          } else {
                              var e = '<a href="javascript:void(0)" onclick="editrow(this)">Edit</a> ';
                              var d = '<a href="javascript:void(0)" onclick="deleterow(this)">Delete</a>';
                              return e + d;
                          }
                      }
                  }
            ]],
            onEndEdit: function (index, row) {
                $.ajax
                  ({
                    type: "GET",
                    url: weburl2 + 'queryType=SqlSp&sp=CRUD_Store_Edit&paramlist=' + row.ID + ',' + row.StoreTypeID + ',' + row.TimeZoneID + ',' + row.HardwareID + ',' + row.Name + ',' + row.Macaddress + ',' + row.IPAddress + ',' + row.CellNumber + ',' + row.Enable + ',' + row.Lat + ',' + row.Long,
                        success: function (html) {
                           $.messager.alert('Success', 'Successfully updated', function (r) { });
                     }
                 });
                var ed = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'StoreTypeID'
                });
                var ed2 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'TimeZoneID'
                });
                var ed3 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'HardwareID'
                });
                var ed4 = $(this).datagrid('getEditor', {
                    index: index,
                    field: 'Enable'
                });
                row.StoreType = $(ed.target).combobox('getText');
                row.Timezone = $(ed2.target).combobox('getText');
                row.Hardware = $(ed3.target).combobox('getText');
                row.Enable = $(ed4.target).combobox('getText');
            },
            onBeforeEdit: function (index, row) {
                row.editing = true;
                $(this).datagrid('refreshRow', index);
            },
            onAfterEdit: function (index, row) {
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            },
            onCancelEdit: function (index, row) {
                row.editing = false;
                $(this).datagrid('refreshRow', index);
            }

        }).datagrid('enableFilter');
        function getRowIndex(target) {
            var tr = $(target).closest('tr.datagrid-row');
            return parseInt(tr.attr('datagrid-row-index'));
        }
        function editrow(target) {
            $('#dg').datagrid('beginEdit', getRowIndex(target));
        }
        function deleterow(target) {
             fams.deletefromtable(getRowIndex(target), $('#dg').datagrid('getData'), 'CRUD_Store_Remove').then(function () {
                $('#dg').datagrid('reload'); 
            });
            //$.messager.confirm('Confirm', 'Are you sure?', function (r) {
            //    if (r) {
            //        var row = $('#dg').treegrid('getSelected');
            //        $.ajax
            //         ({
            //         type: "GET",
            //         url: weburl + 'queryType=SqlSp&sp=CRUD_Store_Remove&paramlist=' + row.ID,
            //         success: function (html) {
            //             $('#dg').datagrid('deleteRow', getRowIndex(target));
            //             var inserted = $('#dg').datagrid('getChanges', 'deleted');
            //             $.messager.alert('Success', 'Successfully removed', function (r) { });
            //         }
            //     });
            //    }
            //});
        }
        function saverow(target) {
            $('#dg').datagrid('endEdit', getRowIndex(target));
          

        }
        function cancelrow(target) {
            $('#dg').datagrid('cancelEdit', getRowIndex(target));
        }
        function insert() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                var index = $('#dg').datagrid('getRowIndex', row);
            } else {
                index = 0;
            }
            $('#dg').datagrid('insertRow', {
                index: index,
                row: {
                    status: 'P'
                }
            });
            $('#dg').datagrid('selectRow', index);
            $('#dg').datagrid('beginEdit', index);
        }    
    </script>
</body>
</html>
