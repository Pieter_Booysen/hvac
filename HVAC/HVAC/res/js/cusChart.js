﻿var cusChart = function () {
    return {
        initDashboardInvHist: function () {
            alert("1");

            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 2000;
            var ssdate = $('#mdate').text();
            var visitors2 = [];
            var siteId = ("1");
            $("#inventoryHistory").text(" START CALL --> END CALL : " + ssdate);
            $.ajax({
                url: "http://csdtecmo.biz/cloud/Handler1.ashx?Client=FAMS2&action=ArrayDataSetToJSON&queryType=SqlSp&sp=get_Inventory_History2&Auth=CSD123456&paramList=20170801,20170807,1",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    visitors2 = response.Table;
                    alert(visitors2);
                },
                failure: function (response) {
                    alert("Error load data");
                }
            });
            ///Must do ajax call here
            if ($('#inventoryHistory').size() != 0) {

         

                var plot_statistics = $.plot($("#inventoryHistory"),
                    [{
                        data: visitors2,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#9f9ff8']
                    }, {
                        data: visitors2,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: " #9f9ff8",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#inventoryHistory").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Start to End Call activities');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });

                $("#inventoryHistory").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#inventoryHistory').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }

        },
       
    }
}